const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  purge: [],
  theme: {
    extend: {},
    screens: {
      xs: "520px",
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1280px",
    },
    maxHeight: {
      "12": "3rem",
      "20": "5rem",
      "24": "6rem",
      "48": "12rem",
      "64": "16rem",
    },
    backgroundSize: {
      "100-200": "100% 200%"
    },
    fontFamily: {
      scout: ["Scout Family", "sans-serif"],
    },
    colors: {
      ...defaultTheme.colors,
      blue: {
        100: "#ebf8ff",
        200: "#bee3f8",
        300: "#90cdf4",
        400: "#63b3ed",
        500: "#007ace",
        600: "#3182ce",
        700: "#2b6cb0",
        800: "#2c5282",
        900: "#2a4365",
      },
      orange: {
        500: "#D9630C"
      },
      gray: {
        '100': '#f5f5f5',
        '200': '#ededed',
        '300': '#e0e0e0',
        '400': '#bdbdbd',
        '500': '#9e9e9e',
        '600': '#757575',
        '700': '#616161',
        '800': '#424242',
        '900': '#212121',
      }
    },
  },
  variants: {
    backgroundPosition: ["hover"]
  },
  plugins: [],
};
