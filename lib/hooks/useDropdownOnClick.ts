/*
    Inspired by the useDropdown hook from @mehiRef;
    * https://gist.github.com/mehiRef/58edfd55d785c133276ac0f6c250bc20
    * https://github.com/mehiRef
 */

import { useState, useEffect, useCallback, useRef, MutableRefObject } from "react";

function useDropdownOnClick<TButtonRef extends HTMLElement, TContentRef extends HTMLElement>(): [
  MutableRefObject<TButtonRef>,
  MutableRefObject<TContentRef>,
  boolean,
  () => void
] {
  const dropdownButtonRef = useRef<TButtonRef>(null);
  const dropdownContentRef = useRef<TContentRef>(null);

  const [isDropdownOpen, setDropdownOpen] = useState(false);

  const toggleDropdown = () => setDropdownOpen(!isDropdownOpen);
  const closeDropdown = () => setDropdownOpen(false);

  const onWindowClick = useCallback(
    (e) => {
      const hasClickedOnDropdownButton =
        e.target === dropdownButtonRef.current || (dropdownButtonRef.current?.contains(e.target) ?? false);
      const hasClickedOnDropdownContent =
        e.target === dropdownContentRef.current || (dropdownContentRef.current?.contains(e.target) ?? false);

      if (!hasClickedOnDropdownButton && !hasClickedOnDropdownContent && isDropdownOpen === true) {
        closeDropdown();
      }
    },
    [isDropdownOpen]
  );

  const onEsc = useCallback(
    (e) => {
      if (e.keyCode === 27 && isDropdownOpen === true) {
        closeDropdown();
      }
    },
    [isDropdownOpen]
  );

  useEffect(() => {
    window.addEventListener("click", onWindowClick);
    return () => window.removeEventListener("click", onWindowClick);
  });

  useEffect(() => {
    window.addEventListener("keyup", onEsc);
    return () => window.removeEventListener("keyup", onEsc);
  });

  return [dropdownButtonRef, dropdownContentRef, isDropdownOpen, toggleDropdown];
}

export default useDropdownOnClick;
