import { useRef, useEffect } from "react";

const useClampText = (text: string) => {
  const elementRef = useRef(null);

  const updateTexts = () => {
    /*
      ATTENTION: the elemet should have a line-height of at least 1.25 for this to work without risking an infinite loop 
    */
    let element = elementRef.current;
    if (element) {
      let textWordArray = text.split(" ");

      element.innerHTML = text;
      while (element.scrollHeight > element.offsetHeight) {
        textWordArray.pop();
        element.innerHTML = textWordArray.join(" ") + "...";
      }
    }
  };

  const rect = elementRef.current?.getBoundingClientRect();

  useEffect(() => {
    updateTexts();
    window.addEventListener("resize", updateTexts);
    return () => window.removeEventListener("resize", updateTexts);
  }, [elementRef.current, text, rect]);

  return elementRef;
};

export default useClampText;
