/*
    Inspired by the useDropdown hook from @mehiRef;
    * https://gist.github.com/mehiRef/58edfd55d785c133276ac0f6c250bc20
    * https://github.com/mehiRef
 */

import { useState, useEffect, useCallback, useRef, MutableRefObject } from "react";

function useDropdownOnHover<TContainerRef extends HTMLElement>(): [
  MutableRefObject<TContainerRef>,
  boolean,
  () => void
] {
  const dropdownContainerRef = useRef<TContainerRef>(null);

  const [isDropdownOpen, setDropdownOpen] = useState(false);

  const toggleDropdown = () => setDropdownOpen(!isDropdownOpen);

  useEffect(() => {
    if (dropdownContainerRef.current) {
      dropdownContainerRef.current.addEventListener("mouseenter", () => setDropdownOpen(true));
      dropdownContainerRef.current.addEventListener("mouseleave", () => setDropdownOpen(false));
    }
  }, [dropdownContainerRef.current])

  return [dropdownContainerRef, isDropdownOpen, toggleDropdown];
}

export default useDropdownOnHover;
