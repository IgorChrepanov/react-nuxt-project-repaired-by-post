import fs from "fs";
import { join } from "path";

const configDirectory = join(process.cwd(), "config");

export function getChampionshipConfig(): any {
  const configFilePath = join(configDirectory, `championship-config.json`);
  const configFileData = fs.readFileSync(configFilePath, "utf8");

  return JSON.parse(configFileData);
}

export function getTwitchChannelConfig(): any {
  const configFilePath = join(configDirectory, `twitch-channel-config.json`);
  const configFileData = fs.readFileSync(configFilePath, "utf8");

  return JSON.parse(configFileData);
}
