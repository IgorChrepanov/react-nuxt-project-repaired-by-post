import { ChannelLiveData } from "../../pages/api/getLiveChannels";
import { TeamScore, AllScores } from "../../pages/api/getTeamScores";

export interface Team {
  name: string;
  imgUrl: string;
  points: number;
}
export interface Leaderboard {
  status: "started" | "ongoing" | "join" | "coming-soon";
  id: string;
  ranking: Team[];
}

export async function getLiveChannels(): Promise<ChannelLiveData[]> {
  const url = "/api/getLiveChannels";
  let channelsData: ChannelLiveData[] = await fetch(url).then((response) => response.json());

  return channelsData;
}
export async function getLeaderboard(championshipId: string): Promise<Leaderboard> {
  const url = `/api/getLeaderBoard/${championshipId}`;
  let { leaderboard }: { leaderboard: Leaderboard } = await fetch(url).then((response) => response.json());

  return leaderboard;
}

export async function getTeamScores(): Promise<AllScores> {
  const url = `/api/getTeamScores`;

  return await fetch(url).then((response) => response.json());
}
export async function getMatchData(): Promise<AllScores> {
  const url = `/api/getMatchData`;

  return await fetch(url).then((response) => response.json());
}
