export interface News {
  title: string;
  excerpt?: string;
  date: string;
  slug: string;
  img: string;
  content?: string;
}
