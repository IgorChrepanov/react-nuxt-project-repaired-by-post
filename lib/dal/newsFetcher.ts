import fs from "fs";
import { join } from "path";
import matter from "gray-matter";

const newsDirectory = join(process.cwd(), "_news");
export interface News {
  title?: string;
  date?: string;
  img?: string;
  excerpt?: string;
  slug?: string;
  content?: string;
}

export function getNewsSlugs(): string[] {
  return fs.readdirSync(newsDirectory);
}

export function getNewsBySlug(slug: string, fields: string[] = []): News {
  const realSlug = slug.replace(/\.md$/, "");
  const fullPath = join(newsDirectory, `${realSlug}.md`);
  const fileContents = fs.readFileSync(fullPath, "utf8");
  const { data, content } = matter(fileContents);

  const items = {};

  // Ensure only the minimal needed data is exposed
  fields.forEach((field) => {
    if (field === "slug") {
      items[field] = realSlug;
    }
    if (field === "content") {
      items[field] = content;
    }
    if (data[field]) {
      items[field] = data[field];
    }
  });

  return items;
}

export function getAllNews(fields: string[] = []): News[] {
  const slugs = getNewsSlugs();
  const news = slugs.map((slug) => getNewsBySlug(slug, fields));

  return news.sort((newsA, newsB) => new Date(newsB.date).getTime() - new Date(newsA.date).getTime());
}

export function getAdjacentNews(
  currentNewsSlug: string,
  fields: string[] = []
): { previousNews: News; nextNews: News } {
  const allNews = getAllNews(fields);
  const indexOfCurrentNews = allNews.indexOf(allNews.find(news => news.slug === currentNewsSlug));
  const previousNews = allNews[indexOfCurrentNews - 1] ?? null;
  const nextNews = allNews[indexOfCurrentNews + 1] ?? null;

  return { previousNews, nextNews };
}
