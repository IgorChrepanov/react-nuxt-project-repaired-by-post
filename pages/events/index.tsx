import React, { useState, useEffect, useRef } from "react";
import withShell from "../../components/Shell";
import { getTwitchChannelConfig } from "../../lib/dal/configFetcher";
import useMedia from "use-media";
import { getLiveChannels } from "../../lib/dal/apiClient";
import { ChannelLiveData } from "../api/getLiveChannels";

interface TwitchEmbedProps {
  channelName: string;
}

const TwitchEmbed: React.FC<TwitchEmbedProps> = ({ channelName }) => {
  const [host, setHost] = useState<string>("localhost");
  const isLarge = useMedia({ minWidth: "1024px" });
  const streamFrameRef = useRef(null);

  useEffect(() => {
    setHost(window.location.host);
  }, []);
  return (
    <div className="flex flex-wrap">
      <div ref={streamFrameRef} className="relative lg:w-4/5 w-full" style={{ paddingBottom: "56.25%" }}>
        <iframe
          src={`https://player.twitch.tv/?channel=${channelName}&parent=${host}&muted=true`}
          className="w-full h-full top-0 left-0 absolute"
          allowFullScreen={true}
        />
      </div>
      <div className="lg:w-1/5 bg-white w-full">
        <iframe
          id="grsoares"
          className="h-half-screen lg:h-full w-full"
          src={`https://www.twitch.tv/embed/${channelName}/chat?parent=${host}`}
        ></iframe>
      </div>
    </div>
  );
};
interface ChannelButtonProps {
  isSelected: boolean;
  isLive: boolean;
  onClick: () => void;
}
const ChannelButton: React.FC<ChannelButtonProps> = ({ isLive, onClick, isSelected, children }) => {
  return (
    <div
      role="button"
      className={`p-2 m-2 border-2 border-gray-200 hover:border-blue-500 max-w-sm ${isSelected ? "bg-blue-500" : "bg-white"}`}
      onClick={onClick}
    >
      <div className={`h-3 w-3 rounded-full ${isLive ? "bg-red-600" : "bg-gray-600"}`} />
      {children}
    </div>
  );
};

const Events = ({ channelConfig }) => {
  let [liveChannels, setLiveChannels] = useState<ChannelLiveData[]>([]);

  useEffect(() => {
    getLiveChannels().then((channels) => setLiveChannels(channels));
  }, []);

  let [selectedChannelName, setSelectedChannelName] = useState<string>(channelConfig.officialChannels[0].name);
  return (
    <div className="bg-gray-200 py-10">
      <div className="container mx-auto">
        <h1 className="text-center font-scout stretch-condensed text-4xl font-bold uppercase">Live Streams</h1>
        <div className="flex flex-wrap w-full items-center">
          <h2 className="font-scout w-full stretch-condensed text-3xl font-bold uppercase">Canais oficiais:</h2>
          <div className="flex justify-center items-start flex-wrap w-full">
            {channelConfig.officialChannels.map((channel, i) => {
              const liveChannelData = liveChannels.find((c) => c.id === channel.id);

              return (
                <ChannelButton
                  key={i}
                  onClick={() => setSelectedChannelName(channel.name)}
                  isSelected={selectedChannelName === channel.name}
                  isLive={liveChannelData?.liveStatus ?? false}
                >
                  <h1 className="b-0 text-center font-scout stretch-condensed text-lg font-bold uppercase">
                    {channel.title}
                  </h1>
                  <h2 className="b-0 text-center font-scout stretch-condensed text-base font-bold uppercase">
                    {liveChannelData?.liveTitle ?? "Offline"}
                  </h2>
                  <h3 className="b-0 text-center font-scout stretch-condensed text-base font-normal uppercase">
                    {liveChannelData?.liveSubTitle ?? ""}
                  </h3>
                </ChannelButton>
              );
            })}
          </div>
        </div>
        <div className="flex flex-wrap w-full items-center mt-10">
          <h2 className="font-scout stretch-condensed text-3xl font-bold uppercase w-full">Canais da comunidade:</h2>
          <div className="flex justify-center items-start flex-wrap w-full">
            {channelConfig.communityChannels.map((channel, i) => {
              const liveChannelData = liveChannels.find((c) => c.id === channel.id);
              return (
                <ChannelButton
                  key={i}
                  onClick={() => setSelectedChannelName(channel.name)}
                  isSelected={selectedChannelName === channel.name}
                  isLive={liveChannelData?.liveStatus ?? false}
                >
                  <h1 className="b-0 text-center font-scout stretch-condensed text-lg font-bold uppercase">
                    {channel.title}
                  </h1>
                  <h2 className="b-0 text-center font-scout stretch-condensed text-base font-bold uppercase">
                    {liveChannelData?.liveTitle ?? "Offline"}
                  </h2>
                  <h3 className="b-0 text-center font-scout stretch-condensed text-base font-normal uppercase">
                    {liveChannelData?.liveSubTitle ?? ""}
                  </h3>
                </ChannelButton>
              )
            })}
          </div>
        </div>
        <TwitchEmbed channelName={selectedChannelName} />
      </div>
    </div>
  );
};

export async function getStaticProps() {
  const channelConfig = getTwitchChannelConfig();

  return {
    props: { channelConfig },
  };
}

export default withShell(Events);
