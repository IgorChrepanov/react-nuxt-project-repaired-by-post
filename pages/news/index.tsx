import React, { useState } from "react";
import withShell from "../../components/Shell";
import MainNews from "../../components/News/MainNews";
import NewsFilter from "../../components/News/NewsFilter";
import NewsList from "../../components/News/NewsList";
import { getAllNews } from "../../lib/dal/newsFetcher";
import PageSelection from "../../components/Shared/PageSelection";
import useMedia from "use-media";



const News = ({ allNews }) => {
  const [firstNews, ...remainingNewsList]: any[] = allNews;
  const [currentPage, setCurrentPage] = useState<number>(1);
  const isWide = useMedia({ minWidth: 1024 });
  const isMedium = useMedia({ minWidth: 768 });
  const isSmall = useMedia({ maxWidth: 525 })
  const newsByPage = 6;
  const newsStart = (currentPage - 1) * newsByPage;
  const newsEnd = Math.min(currentPage * newsByPage, remainingNewsList.length);


  return (
    <div className="bg-gray-200 py-12">
      <div className="mb-12">
        <MainNews news={firstNews} />
      </div>
      {/* {<NewsFilter />} */}
      <NewsList newsList={remainingNewsList.slice(newsStart, newsEnd)} />
      <div className={isSmall ? "px-8" : ""}>
        <PageSelection
          formFactor={isWide ? "EXTENDED" : (isSmall ? "ULTRA-COMPRESSED" : "COMPRESSED")}
          maxPageDisplay={isMedium || isSmall ? 5 : 3}
          currentPage={currentPage}
          setPageNumber={num => setCurrentPage(num)}
          totalNumberOfPages={Math.ceil(remainingNewsList.length / newsByPage)} />
      </div>
    </div>
  );
};

export default withShell(News);

export async function getStaticProps() {
  const allNews = getAllNews(["title", "excerpt", "date", "slug", "img"]);

  return {
    props: { allNews },
  };
}
