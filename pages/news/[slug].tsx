import React from "react";
import { useRouter } from "next/router";
import ErrorPage from "next/error";
import withShell from "../../components/Shell";
import remark from "remark";
import html from "remark-html";
import { getAllNews, getNewsBySlug, getAdjacentNews } from "../../lib/dal/newsFetcher";
import Button from "../../components/Shared/Button";
import { News } from "../../lib/dal/news";
import FacebookShareButton from "../../components/Shared/FacebookShareButton";
import TwitterShareButton from "../../components/Shared/TwitterShareButton";
import Link from "next/link";
import useClampText from "./../../lib/hooks/useClampText";

const RecommendedNews: React.FC<{ news: News }> = ({ news }) => {
  const excerptRef = useClampText(news.excerpt);
  const titleRef = useClampText(news.title);

  const newsDate = new Date(news.date);
  return (
    <div className="flex flex-col w-full md:w-1/2 lg:w-1/3 bg-white m-8 mt-0">
      <img src={news.img} className="object-cover h-full w-full" />
      <div className="p-4">
        <h4 className="font-bold text-blue-500 mb-1 text-lg font-scout">
          {("0" + newsDate.getDate()).slice(-2)} {("0" + (newsDate.getMonth() + 1)).slice(-2)} {newsDate.getFullYear()}
        </h4>
        <Link href="/news/[slug]" as={"/news/" + news.slug}>
          <a>
            <h2
              ref={titleRef}
              className="overflow-hidden font-bold mb-1 text-lg xs:text-xl sm:text-2xl lg:text-3xl xl:text-4xl uppercase max-h-12 lg:max-h-24 hover:underline font-scout stretch-condensed leading-tight"
            >
              {news.title}
            </h2>
          </a>
        </Link>
        <p ref={excerptRef} className="overflow-hidden h-20 mb-3 leading-tight font-scout">
          {news.excerpt}
        </p>
        <Link href="/news/[slug]" as={"/news/" + news.slug}>
          <a>
            <Button>
              <span className="text-2xl">
                Read More
              </span>
            </Button>
          </a>
        </Link>
      </div>
    </div>
  );
};

interface NewsPageProps {
  news: News;
  previousNews?: News;
  nextNews?: News;
}

const NewsPage: React.FC<NewsPageProps> = ({ news, previousNews, nextNews }) => {
  const router = useRouter();

  if (!router.isFallback && !news?.slug) {
    return <ErrorPage statusCode={404} />;
  }

  const newsDate = new Date(news.date);

  return (
    <>
      <div className="bg-gray-200 pb-12">
        <div className="container mx-auto">
          <div className="flex flex-wrap">
            <div className="flex justify-center w-1/2 order-2 mt-8 lg:mt-64 lg:pr-4 lg:order-first lg:w-auto lg:w-1/6 lg:justify-end">
              {previousNews ? (
                <Link href="/news/[slug]" as={"/news/" + previousNews.slug}>
                  <a>
                    <Button outline>{"<"}</Button>
                  </a>
                </Link>
              ) : (
                  ""
                )}
            </div>
            <div className="w-full lg:w-4/6">
              <Link href={`/news`}>
                <a>
                  <Button customClassName="my-8" outline>
                    {"< Voltar"}
                  </Button>
                </a>
              </Link>
              <div className="bg-white py-12 px-10">
                <div className="px-4">
                  <h1 className="font-bold mb-3 text-3xl xs:text-4xl sm:text-5xl lg:text-6xl uppercase font-scout stretch-condensed leading-none">
                    {news.title}
                  </h1>
                  <h4 className="font-bold text-blue-500 mb-3 text-lg font-scout">
                    {("0" + newsDate.getDate()).slice(-2)} {("0" + (newsDate.getMonth() + 1)).slice(-2)}{" "}
                    {newsDate.getFullYear()}
                  </h4>
                </div>
                <img src={news.img} className="w-full" />
                <span
                  className="px-4"
                  dangerouslySetInnerHTML={{
                    __html: news.content,
                  }}
                ></span>
              </div>
            </div>
            <div className="flex justify-center w-1/2 order-3 mt-8 lg:mt-64 lg:pl-4 lg:order-last lg:w-1/6 lg:justify-start">
              {nextNews ? (
                <Link href="/news/[slug]" as={"/news/" + nextNews.slug}>
                  <a>
                    <Button outline>{">"}</Button>
                  </a>
                </Link>
              ) : (
                  ""
                )}
            </div>
          </div>
        </div>
        <div className="flex justify-center mt-12">
          <FacebookShareButton />
          <TwitterShareButton />
        </div>
      </div>
      <div className="bg-gray-400 pt-8">
        <h1 className="mt-0 mb-8 text-5xl font-bold text-center uppercase font-scout stretch-condensed">
          Coteúdo Recomendado
        </h1>
        <div className="flex flex-col md:flex-row items-center justify-center">
          {previousNews && <RecommendedNews news={previousNews} />}
          {nextNews && <RecommendedNews news={nextNews} />}
        </div>
      </div>
    </>
  );
};

export async function getStaticProps({ params }) {
  const news = getNewsBySlug(params.slug, ["title", "date", "slug", "content", "img"]);
  const content = (
    await remark()
      .use(html)
      .process(news.content || "")
  ).toString();

  const { previousNews, nextNews } = getAdjacentNews(params.slug, ["title", "date", "slug", "excerpt", "img"]);

  return {
    props: {
      news: {
        ...news,
        content,
      },
      previousNews,
      nextNews,
    },
  };
}

export async function getStaticPaths() {
  const news = getAllNews(["slug"]);

  return {
    paths: news.map((currentNews) => {
      return {
        params: {
          slug: currentNews.slug,
        },
      };
    }),
    fallback: false,
  };
}

export default withShell(NewsPage);
