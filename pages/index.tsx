import withShell from "../components/Shell";
import React, { useState, useRef, useEffect } from "react";
import LatamLeague from "./../components/HomeTabs/LatamLeague";
import LeagueTab from "../components/HomeTabs/LeagueTab";
import { getAllNews } from "../lib/dal/newsFetcher";
import { getChampionshipConfig } from "../lib/dal/configFetcher";
import { TeamScore, AllScores } from "./api/getTeamScores";
import { getTeamScores, getMatchData } from "../lib/dal/apiClient";
import { useRouter } from "next/router";
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonBack,
  ButtonNext,
} from "pure-react-carousel";
type TabSelection = "LATAM LEAGUE" | "BRAZIL" | "MEXICO" | "SOUTH";
const TabTriangle: React.FC<{ leftMargin: number }> = ({ leftMargin }) => (
  <svg
    style={{
      position: "relative",
      top: "-12px",
      marginBottom: "-12px",
      transition: "left 0.3s ease",
      left: `${leftMargin}px`,
    }}
    id="tabTriangle"
    width="12"
    height="12"
    viewBox="0 0 116 100"
    xmlns="http://www.w3.org/2000/svg"
    className="fill-current text-blue-700"
  >
    <path fillRule="evenodd" clipRule="evenodd" d="M57.5 0L115 100H0L57.5 0z" />
  </svg>
);

interface TabsHeaderTabProps {
  onClick: () => void;
  isSelected: boolean;
  setTabTriangleLeftMargin: (number) => void;
}

const TabsHeaderTab: React.FC<TabsHeaderTabProps> = (props) => {
  const liEl = useRef<HTMLLIElement>(null);
  let boundingRect = liEl.current?.getBoundingClientRect();

  const updateTrianglePosition = () => {
    boundingRect = liEl.current?.getBoundingClientRect();
    if (props.isSelected && boundingRect) {
      props.setTabTriangleLeftMargin(
        boundingRect.left + boundingRect.width / 2
      );
    }
  };

  useEffect(() => {
    updateTrianglePosition();
    window.addEventListener("resize", updateTrianglePosition);
    return () => window.removeEventListener("resize", updateTrianglePosition);
  }, [props.isSelected, boundingRect]);
  return (
    <li
      ref={liEl}
      className="flex items-center text-center py-2 mx-4 text-lg sm:mx-8 md:mx-10 lg:mx-14 sm:text-xl md:text-2xl lg:text-3xl text-gray-700 font-scout font-bold cursor-pointer"
      onClick={props.onClick}
    >
      <a className={`${props.isSelected ? "text-blue-700" : ""}`}>
        {props.children}
      </a>
    </li>
  );
};

interface TabsHeaderProps {
  setTabSelection: (tab: TabSelection) => void;
  tabSelection: TabSelection;
  teamScores: AllScores;
  matchData: any;
}

const TabsHeader: React.FC<TabsHeaderProps> = (props) => {
  const [tabTriangleLeftMargin, setTabTriangleLeftMargin] = useState(0);
  const [displayTriangle, setDisplayTriangle] = useState(false);

  useEffect(() => {
    setDisplayTriangle(true);
  }, []);

  const router = useRouter();

  return (
    <>
      <video
        loop
        muted
        autoPlay
        className="main-video absolute z-0 left-0 min-w-full min-h-full w-auto h-auto"
        style={{
          top: "-50%",
        }}
      >
        <source src="latam-movie.mp4" type="video/mp4" />
        <source src="latam-movie.webm" type="video/webm" />
        <source src="latam-movie.ogv" type="video/ogg" />
      </video>
      <div className="flex justify-between items-center content-container relative">
        <div
          style={{
            width: "22%",
            height: "100%",
            backgroundImage:
              "linear-gradient(90deg,hsla(0,0%,100%,.75),hsla(0,0%,100%,.6) 33%,hsla(0,0%,100%,.1) 80%,hsla(0,0%,100%,0))",
          }}
          className="main-content absolute z-10 text-white flex justify-center flex-col px-5"
        >
          <div>
            <h1 className="text-2xl">
              O CAMPEONATO BRASILIERO ENTRA NA SUA ULTIMA SEMANA
            </h1>
          </div>
          <div>
            <p>
              COMOS JOGOS TODAS AS QUINTAS SABADOSE DOMNIGO, CONFIRA A TABELA DE
              CLASSIFICACAO
            </p>
          </div>
          <div>
            <button className="bg-blue-500 text-white px-5 py-2 shadow-lg rounded-sm">
              CONFIRAAS NOVIDADES
            </button>
          </div>
        </div>

        {/*TODO: Remove the double rendering using the navigator, check for the appVersion */}
        <div style={{ display: "none" }} className="left-content">
          <BannerContent router={router} bannerClassName="" />
        </div>
        <BannerContent bannerClassName="lg-device" />
      </div>
      <CarouselProvider
        className="relative"
        naturalSlideWidth={100}
        naturalSlideHeight={125}
        totalSlides={props.matchData?.length}
        visibleSlides={1.6}
      >
        <Slider>
          <div
            className="flex"
            style={{ background: "#f7f8f9", height: "100px" }}
          >
            {props.matchData?.map((match, index) => {
              return (
                <div
                  key={index}
                  className="match-container items-center p-4 flex-row flex items-center"
                  style={{ minWidth: "750px" }}
                >
                  <div className="date p-4">{match.date}</div>
                  {Object.keys(match).map((key, index) => {
                    if (key == "date") return "";
                    return (
                      <Slide index={index}>
                        <div
                          key={`${index}a`}
                          onClick={() => router.push("/events")}
                          className="match flex w-full bg-white shadow-xs p-6 items-center"
                          style={{ height: "70px" }}
                        >
                          <div id="match1" className="flex w-full flex-col">
                            <div className="time" style={{ fontSize: "10px" }}>
                              {match[key].time}
                            </div>
                            <div className="match-info text-xs flex w-full justify-between">
                              <div className="flex">
                                <img
                                  width="20px"
                                  height="20px"
                                  className="p-1"
                                  src={match[key].team1Image}
                                />
                                <div>{match[key].team1Name}</div>
                              </div>
                              <div>{match[key].team1Score}</div>
                            </div>
                            <div className="match-info text-xs flex w-full justify-between">
                              <div className="flex">
                                <img
                                  width="20px"
                                  className="p-1"
                                  height="20px"
                                  src={match[key].team2Image}
                                />
                                <div>{match[key].team2Name}</div>
                              </div>
                              <div>{match[key].team2Score}</div>
                            </div>
                          </div>
                        </div>
                      </Slide>
                    );
                  })}
                </div>
              );
            })}
          </div>
        </Slider>
        <ButtonBack className="absolute left-0 top-0 h-full bg-blue-200 text-white text-4xl p-1 shadow-xs">
          {"<"}
        </ButtonBack>
        <ButtonNext className="absolute right-0 top-0 h-full bg-blue-200 text-white text-4xl p-1 shadow-xs">
          {">"}
        </ButtonNext>
      </CarouselProvider>

      <ul className="flex justify-center border-b-2 border-blue-700">
        <TabsHeaderTab
          onClick={() => props.setTabSelection("LATAM LEAGUE")}
          isSelected={props.tabSelection === "LATAM LEAGUE"}
          setTabTriangleLeftMargin={setTabTriangleLeftMargin}
        >
          LATAM LEAGUE
        </TabsHeaderTab>
        <TabsHeaderTab
          onClick={() => props.setTabSelection("BRAZIL")}
          isSelected={props.tabSelection === "BRAZIL"}
          setTabTriangleLeftMargin={setTabTriangleLeftMargin}
        >
          BRAZIL
        </TabsHeaderTab>
        <TabsHeaderTab
          onClick={() => props.setTabSelection("MEXICO")}
          isSelected={props.tabSelection === "MEXICO"}
          setTabTriangleLeftMargin={setTabTriangleLeftMargin}
        >
          MEXICO
        </TabsHeaderTab>
        <TabsHeaderTab
          onClick={() => props.setTabSelection("SOUTH")}
          isSelected={props.tabSelection === "SOUTH"}
          setTabTriangleLeftMargin={setTabTriangleLeftMargin}
        >
          SOUTH
        </TabsHeaderTab>
      </ul>
      {displayTriangle && <TabTriangle leftMargin={tabTriangleLeftMargin} />}
    </>
  );
};

const getScoreFromRankingPositionTwoStages: (number) => number = (
  rankingPosition: number
) => {
  if (rankingPosition >= 17) {
    return 5;
  } else if (rankingPosition >= 9) {
    return 10;
  } else if (rankingPosition >= 5) {
    return 35;
  } else if (rankingPosition >= 3) {
    return 45;
  } else if (rankingPosition === 2) {
    return 70;
  } else if (rankingPosition === 1) {
    return 100;
  }

  return -1;
};

const getScoreFromRankingPositionFourStages: (number) => number = (
  rankingPosition: number
) => {
  if (rankingPosition >= 17) {
    return 0;
  } else if (rankingPosition >= 9) {
    return 5;
  } else if (rankingPosition >= 5) {
    return 25;
  } else if (rankingPosition >= 3) {
    return 50;
  } else if (rankingPosition === 2) {
    return 75;
  } else if (rankingPosition === 1) {
    return 100;
  }

  return -1;
};

const Home = ({
  allNews,
  championshipConfig,
}: {
  allNews: any[];
  championshipConfig: any;
}) => {
  const [tabSelection, setTabSelection] = useState<TabSelection>(
    "LATAM LEAGUE"
  );
  const [teamScores, setTeamScores] = useState<AllScores>(null);
  const [matchData, setMatchData] = useState<AllScores>(null);
  useEffect(() => {
    getTeamScores().then((scores) => setTeamScores(scores));
  }, []);
  useEffect(() => {
    getMatchData().then((matchData) => setMatchData(matchData));
  }, []);

  return (
    <>
      <TabsHeader
        teamScores={teamScores}
        matchData={matchData}
        setTabSelection={(tab: TabSelection) => setTabSelection(tab)}
        tabSelection={tabSelection}
      />
      <div className="bg-gray-200">
        <span className={tabSelection !== "LATAM LEAGUE" ? "hidden" : ""}>
          <LatamLeague news={allNews[0]} />
        </span>
        <LeagueTab
          hidden={tabSelection !== "BRAZIL"}
          championshipRankingTitle="Campeonato Brasileiro"
          allNews={allNews}
          scoringFunction={getScoreFromRankingPositionTwoStages}
          championshipIds={championshipConfig.brazil}
          teamScores={teamScores?.brazil}
        />
        <LeagueTab
          hidden={tabSelection !== "MEXICO"}
          championshipRankingTitle="Campeonato Mexicano"
          allNews={allNews}
          scoringFunction={getScoreFromRankingPositionFourStages}
          championshipIds={championshipConfig.mexico}
          teamScores={teamScores?.mexico}
        />
        <LeagueTab
          hidden={tabSelection !== "SOUTH"}
          championshipRankingTitle="Campeonato Sul-americano"
          allNews={allNews}
          scoringFunction={getScoreFromRankingPositionFourStages}
          championshipIds={championshipConfig.south}
          teamScores={teamScores?.south}
        />
      </div>
    </>
  );
};

const BannerContent: { bannerClassName: string; router: any } = (props) => (
  <>
    <div
      className={`w-full relative overflow-hidden padding-0-imp ${props.bannerClassName}`}
      style={{ paddingTop: "25%" }}
    >
      <div className="absolute h-full z-10 right-0 flex justify-center top-0 items-center content-bg">
        <img
          width="100%"
          style={{ height: "320px" }}
          src="https://static-dm.akamaized.net/r6-esports/prod/r6s-esports-home-bg-header-sidepromo.png"
        />
      </div>
    </div>
    <div
      style={{ width: "20%" }}
      className={`absolute z-10 right-0 content-logo ${props.bannerClassName}`}
    >
      <div className="absolute p-4 text-white">
        <p> O CAMPEONATO BRASILIERO ENTRA NA SUA ULTIMA SEMANA</p>
      </div>
      <img src="https://i.imgur.com/lHRJ19O.png" />{" "}
      <div
        className="absolute z-0 text-center  "
        style={{ width: "100%", bottom: "20px" }}
      >
        <button
          onClick={() => props.router.push("/events")}
          className="bg-blue-500 text-white px-5 py-2 shadow-lg rounded-sm"
        >
          EVENTOS
        </button>
      </div>
    </div>
  </>
);

export async function getStaticProps() {
  const championshipConfig = getChampionshipConfig();

  const allNews = getAllNews(["title", "slug", "excerpt", "img", "date"]);

  return {
    props: { allNews, championshipConfig },
  };
}

const getButtonStyle = (right) => ({
  position: "absolute",
  background: "rgb(175 175 175 / 41%)",
  height: "100%",
  top: 0,
  right: right && 0,
  left: !right && 0,
});
export default withShell(Home);
