import teamLogosConfig from "../../config/team-logos-config.json";
import { NextApiRequest, NextApiResponse } from "next";

const sheetsApiKey = "AIzaSyDUiStw-6cPsEyNXT4HXDtkhs1UfVKAJnY";
const baseUrl = "https://sheets.googleapis.com/v4/spreadsheets";

const im = {
  LIQUID: "Team_Liquid_2020.png",
  "TEAM ONE": "TeamOneLogo.png",
  FAZE: "FaZe_Clan_2018.png",
  NIP: "Ninjas_in_Pyjamas_2017.png",
  MIBR: "MIBR_2018.png",
  W7M: "W7M_Gaming_2018.png",
  "BLACK DRAGONS": "BlackDragons_logo.png",
  INTZ: "INTZ_2019.png",
  SANTOS: "Santos_e-Sports_2019.png",
  FURIA: "Furia_eSports.png",
  "ESTRAL ESPORTS": "Estral_esports_may_2020_logo_teamcard.png",
  "TIMBERS ESPORTS": "Timbers_Esports.png",
  "ATHERIS ESPORTS": "Atheris_esports_logo.png",
  "PIXEL ESPORTS": "Pixel_esports_clublogo_2019.png",
  "CHIVAS ESPORTS": "Chivas_esports_notext_logo.png",
  "INFINITY ACADEMY": "Infinity_academy_logo.png",
  "MEXICAN ESPORTS TEAM": "MeT_logo.png",
  "COSCU ARMY": "Coscu_Army.png",
  "MALVINAS GAMING": "Malvinas_gaming_Logo_2019.png",
  "9Z": "9z_Team.png",
  "FURIOUS GAMING": "Furious_Gaming.png",
  "LBS ESPORTS": "LBS_Esports_logo.png",
  "FLOW NOCTURNS GAMING": "Nocturns_Gaming_2018.png",
  "INFAMOUS GAMING": "INFAMOUS-COMERCIAL.png",
  "AZULES ESPORTS": "Azules_esportslogo.png",
  TBD: "TBD.png",
};

export type TeamScore = {
  teamLogoUrl: string;
  teamName: string;
  points: number;
  gamesPlayed: number;
  mapsDifference: number;
  mapsWon: number;
  roundsDifference: number;
  roundsWon: number;
};

export type AllScores = {
  mexico: TeamScore[];
  south: TeamScore[];
  brazil: TeamScore[];
};

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const sheetId = "1Kwet2HS17l4dW0tBA4hdQkb_tzWCBtOL2pK5_I2hga8";
  const southSheetName = "Schedule";
  const southScoreRange = "A1:ET3";
  const southScoresUrl = `${baseUrl}/${sheetId}/values/${southSheetName}!${southScoreRange}?key=${sheetsApiKey}`;

  const southScores = await fetch(southScoresUrl).then((data) => data.json());
  const schedule = splitIntoChunks(
    southScores.values[0].filter((x) => x),
    4
  );
  const team1 = splitIntoChunks(southScores.values[1], 10);
  const team2 = splitIntoChunks(southScores.values[2], 10);

  res.json(mapData(team1, { schedule, team1, team2 }));
};

function splitIntoChunks(array, chunkSize) {
  return [].concat.apply(
    [],
    array.map(function (elem, i) {
      return i % chunkSize ? [] : [array.slice(i, i + chunkSize)];
    })
  );
}

const mapData = (score, allData) => {
  return score.map((v, i) => {
    const match1 = {
      date: allData.schedule[i][0],
      time: allData.schedule[i][1],
      team1Score: v[3],
      team1Name: v[2],
      team1Image: `./team_logos/${im[v[2]]}`,
      team2Score: allData.team2[i][3],
      team2Image: `./team_logos/${im[allData.team2[i][2]]}`,
      team2Name: allData.team2[i][2],
    };
    const match2 = {
      date: allData.schedule[i][0],
      time: allData.schedule[i][2],
      team1Score: v[6],
      team1Name: v[5],
      team1Image: `./team_logos/${im[v[5]]}`,
      team2Score: allData.team2[i][6],
      team2Name: allData.team2[i][5],
      team2Image: `./team_logos/${im[allData.team2[i][5]]}`,

    };
    const match3 = {
      date: allData.schedule[i][0],
      time: allData.schedule[i][3],
      team1Score: v[9],
      team1Name: v[8],
      team1Image: `./team_logos/${im[v[8]]}`,
      team2Score: allData.team2[i][9],
      team2Name: allData.team2[i][8],
      team2Image: `./team_logos/${im[allData.team2[i][8]]}`,
    };

    return { date: allData.schedule[i][0], match1, match2, match3 };
  });
};
