import { NextApiRequest, NextApiResponse } from "next";

const apiKey = "eda97b76-699d-413b-b4d2-6e8825bde223";
const baseUrl = "https://open.faceit.com/data/v4/";

const limit = 32;

type Team = {
  name: string;
  imgUrl: string;
  points: number;
};

type Leaderboard = {
  status: "started" | "ongoing" | "join" | "coming-soon";
  id: string;
  ranking: Team[];
};

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const {
    query: { championshipId },
  } = req;

  const headers = new Headers({ Authorization: "Bearer " + apiKey });
  const httpConf = {
    method: "GET",
    headers,
  };

  res.setHeader("Cache-Control", "s-maxage=5");

  const leaderbordUrl = baseUrl + "leaderboards/championships/" + championshipId;

  let leaderboardData = await fetch(leaderbordUrl, httpConf).then((response) => response.json());

  if (leaderboardData.items.length === 0) {
    res.json({ leaderboard: null });
    return;
  }

  let status = leaderboardData.items[0].status.toLowerCase();
  let id = leaderboardData.items[0].id;

  if (status != "join" && status != "started" && status != "ongoing" && status != "finished") {
    res.json({ leaderboard: null });
    return;
  }

  const leaderboardRankingUrl = `${baseUrl}leaderboards/${leaderboardData.items[0].leaderboard_id}?limit=${limit}`;
  let leaderboardRankingData = await fetch(leaderboardRankingUrl, httpConf).then((response) => response.json());

  let teams: Team[] = leaderboardRankingData.items.map((faceitPlayer) => {
    return { name: faceitPlayer.player.nickname, imgUrl: faceitPlayer.player.avatar, points: faceitPlayer.points };
  });

  res.json({
    leaderboard: {
      status,
      id,
      ranking: teams,
    },
  });
};
