import { NextApiRequest, NextApiResponse } from "next";
import TwitchChannelConfig from "../../config/twitch-channel-config.json";

const clientId = "a31u7d2go27hjq4owl9v0r89l1kges";
const baseUrl = "https://api.twitch.tv/kraken/";

type ChannelData = {
  _id: string;
  stream_type: string;
  channel: { _id: string, status: string, description: string };
};

export type ChannelLiveData = {
  id: string,
  liveStatus: boolean,
  liveTitle: string,
  liveSubTitle: string
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const headers = new Headers({ "Client-ID": clientId, Accept: "application/vnd.twitchtv.v5+json" });
  const httpConf = {
    method: "GET",
    headers,
  };

  const channels = [...TwitchChannelConfig.officialChannels, ...TwitchChannelConfig.communityChannels];

  const streamUrl = baseUrl + "streams?channel=" + channels.map((c) => c.id).join(",");
  let channelsData: ChannelData[] = await fetch(streamUrl, httpConf)
    .then((response) => response.json())
    .then((data) => data.streams);

  res.setHeader("Cache-Control", "s-maxage=5");

  res.json(
    channelsData.map((data) => {
      const result: ChannelLiveData = {
        id: data.channel._id + "",
        liveStatus: data.stream_type === "live",
        liveTitle: data.channel.status,
        liveSubTitle: data.channel.description
      };
      return result;
    })
  );
};
