import teamLogosConfig from "../../config/team-logos-config.json";
import { NextApiRequest, NextApiResponse } from "next";

const sheetsApiKey = 'AIzaSyDUiStw-6cPsEyNXT4HXDtkhs1UfVKAJnY';
const baseUrl = 'https://sheets.googleapis.com/v4/spreadsheets';

export type TeamScore = {
  teamLogoUrl: string,
  teamName: string,
  points: number,
  gamesPlayed: number,
  mapsDifference: number,
  mapsWon: number,
  roundsDifference: number,
  roundsWon: number
}

export type AllScores = {
  mexico: TeamScore[],
  south: TeamScore[],
  brazil: TeamScore[]
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const sheetId = "1Kwet2HS17l4dW0tBA4hdQkb_tzWCBtOL2pK5_I2hga8";

  const brazilSheetName = "Standings";
  const brazilScoreRange = "C2:I11"
  const brazilScoresUrl = `${baseUrl}/${sheetId}/values/${brazilSheetName}!${brazilScoreRange}?key=${sheetsApiKey}`;

  const brazilScores = await fetch(brazilScoresUrl)
    .then(data => data.json());

  const mexicoSheetName = "Standings(MX)";
  const mexicoScoreRange = "C2:I8"
  const mexicoScoresUrl = `${baseUrl}/${sheetId}/values/${mexicoSheetName}!${mexicoScoreRange}?key=${sheetsApiKey}`;

  const mexicoScores = await fetch(mexicoScoresUrl)
    .then(data => data.json());

  const southSheetName = "Standings(South)";
  const southScoreRange = "C2:I9"
  const southScoresUrl = `${baseUrl}/${sheetId}/values/${southSheetName}!${southScoreRange}?key=${sheetsApiKey}`;

  const southScores = await fetch(southScoresUrl)
    .then(data => data.json());

  const mapSheetToScore = score => {
    const teamScore: TeamScore = {
      teamLogoUrl: `./team_logos/${teamLogosConfig[score[0]]}`,
      teamName: score[0],
      points: parseInt(score[1]),
      gamesPlayed: parseInt(score[2]),
      mapsDifference: parseInt(score[3]),
      mapsWon: parseInt(score[4]),
      roundsDifference: parseInt(score[5]),
      roundsWon: parseInt(score[6])
    }
    return teamScore;
  };

  const scores: AllScores = {
    brazil: brazilScores.values.map(mapSheetToScore),
    south: southScores.values.map(mapSheetToScore),
    mexico: mexicoScores.values.map(mapSheetToScore)
  }

  res.json(scores);
}
