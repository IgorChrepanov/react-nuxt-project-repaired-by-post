import React from "react";
import useMedia from "use-media";

import {
  FaFacebookF,
  FaTwitter,
  FaRedditAlien,
  FaInstagram,
  FaYoutube,
  FaTwitch,
  FaDiscord,
  FaMixer,
} from "react-icons/fa";
import { IoMdChatboxes } from "react-icons/io";

type SocialNetwork =
  | "Facebook"
  | "Twitter"
  | "Reddit"
  | "Instagram"
  | "Forum"
  | "YouTube"
  | "Twitch"
  | "Discord"
  | "Mixer";

interface SocialNetworkLinkProps {
  socialNetowrk: SocialNetwork;
}

const SocialNetworkLink: React.FC<SocialNetworkLinkProps> = ({ socialNetowrk }) => {
  let icon;
  let href = "";

  let isWide = useMedia({ minWidth: 640 });

  switch (socialNetowrk) {
    case "Reddit":
      icon = <FaRedditAlien size={isWide ? 32 : 16} />;
      href = "https://www.reddit.com/r/rainbow6";
      break;
    case "Facebook":
      icon = <FaFacebookF size={isWide ? 32 : 16} />;
      href = "https://www.facebook.com/Rainbow6BR";
      break;
    case "Twitter":
      icon = <FaTwitter size={isWide ? 32 : 16} />;
      href = "https://twitter.com/rainbow6br";
      break;
    case "Instagram":
      icon = <FaInstagram size={isWide ? 32 : 16} />;
      href = "https://www.instagram.com/rainbow6br/";
      break;
    case "Forum":
      icon = <IoMdChatboxes size={isWide ? 32 : 16} />;
      href = "https://forums.ubisoft.com/forumdisplay.php/717-Rainbow-Six-Siege";
      break;
    case "YouTube":
      icon = <FaYoutube size={isWide ? 32 : 16} />;
      href = "https://www.youtube.com/channel/UCFYYhd9-VxkHnaA5cOiSybA";
      break;
    case "Twitch":
      icon = <FaTwitch size={isWide ? 32 : 16} />;
      href = "https://www.twitch.tv/rainbow6br";
      break;
    case "Discord":
      icon = <FaDiscord size={isWide ? 32 : 16} />;
      href = "https://discord.gg/rainbow6";
      break;
    case "Mixer":
      icon = <FaMixer size={isWide ? 32 : 16} />;
      href = "https://www.mixer.com/Rainbow6";
      break;
    default:
  }

  return (
    <a
      className="m-1 cursor-pointer border border-black hover:border-blue-500 p-2"
      style={{
        transition: "border-color 0.1s linear",
      }}
      href={href}
      target="_blank"
    >
      {icon}
    </a>
  );
};

const Footer: React.FC = () => {
  return (
    <div className="bg-black p-8 text-white">
      <h3 className="font-scout uppercase text-2xl text-center font-light">Encontre-nos:</h3>
      <div className="flex justify-center">
        <SocialNetworkLink socialNetowrk="Reddit" />
        <SocialNetworkLink socialNetowrk="Facebook" />
        <SocialNetworkLink socialNetowrk="Twitter" />
        <SocialNetworkLink socialNetowrk="Instagram" />
        <SocialNetworkLink socialNetowrk="Forum" />
        <SocialNetworkLink socialNetowrk="YouTube" />
        <SocialNetworkLink socialNetowrk="Twitch" />
        <SocialNetworkLink socialNetowrk="Discord" />
        <SocialNetworkLink socialNetowrk="Mixer" />
      </div>
      <div className="flex flex-wrap container mx-auto my-8 justify-around">
        <a
          href="https://www.ubisoft.com/"
          className="w-1/4 opacity-75 hover:opacity-100"
          style={{ transition: "opacity .2s ease" }}
        >
          <img src="/ubisoft-logo.webp" className="mx-auto" />
        </a>
        <a
          href="http://store.ubi.com/"
          className="w-1/4 opacity-75 hover:opacity-100"
          style={{ transition: "opacity .2s ease" }}
        >
          <img src="/ubisoft-store-logo.webp" className="mx-auto" />
        </a>
        <a
          href="https://blog.ubi.com/"
          className="w-1/4 opacity-75 hover:opacity-100"
          style={{ transition: "opacity .2s ease" }}
        >
          <img src="/ubisoft-blog-logo.webp" className="mx-auto" />
        </a>
        <a
          href="https://club.ubisoft.com/"
          className="w-1/4 opacity-75 hover:opacity-100"
          style={{ transition: "opacity .2s ease" }}
        >
          <img src="/ubisoft-club-logo.webp" className="mx-auto" />
        </a>
        <a
          href="https://uplay.ubi.com/"
          className="w-1/4 opacity-75 hover:opacity-100"
          style={{ transition: "opacity .2s ease" }}
        >
          <img src="/uplay-logo.webp" className="mx-auto" />
        </a>
        <a
          href="http://www.xbox.com/"
          className="w-1/4 opacity-75 hover:opacity-100"
          style={{ transition: "opacity .2s ease" }}
        >
          <img src="/xbox-one-logo.webp" className="mx-auto" />
        </a>
        <a
          href="http://www.playstation.com/"
          className="w-1/4 opacity-75 hover:opacity-100"
          style={{ transition: "opacity .2s ease" }}
        >
          <img src="/ps4-logo.webp" className="mx-auto" />
        </a>
        <a
          href="https://news.ubisoft.com/en-us/article/6y1Bdln8mi44NSq3LBqFTC/rainbow-six-siege-pc-specs"
          className="w-1/4 opacity-75 hover:opacity-100"
          style={{ transition: "opacity .2s ease" }}
        >
          <img src="/pc-dvd-logo.webp" className="mx-auto" />
        </a>
        <a
          href="https://store.ubi.com/us/clothing/brands/six-siege/"
          className="w-1/4 opacity-75 hover:opacity-100"
          style={{ transition: "opacity .2s ease" }}
        >
          <img src="/ubiworkshop-logo.webp" className="mx-auto" />
        </a>
        <a
          href="http://portal.mj.gov.br/ClassificacaoIndicativa/jsps/ConsultarJogoForm.do"
          className="w-1/4 opacity-75 hover:opacity-100"
          style={{ transition: "opacity .2s ease" }}
        >
          <img src="/mature-17-logo.webp" className="mx-auto" />
        </a>
      </div>
      <div className="container mx-auto">
        <p className="text-center text-xs mx-0 sm:mx-20 md:mx-32 lg:mx-40 font-scout font-thin">
          © 2020 Ubisoft Entertainment. All Rights Reserved. Tom Clancy’s, Rainbow Six, the Soldier Icon, Uplay, the
          Uplay logo, Ubi.com, Ubisoft, and the Ubisoft logo are trademarks of Ubisoft Entertainment in the US and/or
          other countries. The “PS” Family logo is a registered trademark and “PS4” is a trademark of Sony Computer
          Entertainment Inc. Software platform logo (TM and ©) EMA 2006.
        </p>
        <div className="flex justify-center text-sm font-scout font-thin">
          <a href="https://legal.ubi.com/InterestBasedAdvertising/pt-BR" className="uppercase mx-1">
            Publicidade Baseada No Interesse
          </a>
          |
          <a href="https://legal.ubi.com/termsofuse/pt-BR" className="uppercase mx-1">
            Termos de uso
          </a>
          |
          <a href="https://legal.ubi.com/privacypolicy/pt-BR" className="uppercase mx-1">
            Política de privacidade
          </a>
          |
          <a href="https://rainbow6.ubisoft.com/siege/pt-br/news/181-326395-16/" className="uppercase mx-1">
            Código de conduta
          </a>
          |
          <a
            href="https://r6academy.ubi.com/en/credits"
            className="uppercase mx-1"
            dangerouslySetInnerHTML={{
              __html: "<!-- Created with <3 using Next.js by https://twitter.com/_grsoares -->Credits",
            }}
          ></a>
        </div>
      </div>
    </div>
  );
};

export default Footer;
