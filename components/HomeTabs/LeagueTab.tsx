import { useEffect, useState } from "react";
import { News } from "../../lib/dal/news";
import { getLeaderboard, Leaderboard } from "../../lib/dal/apiClient";
import NewsBlock from "../News/NewsBlock";
import PageSelection from "../Shared/PageSelection";
import useMedia from "use-media";
import ChampionshipRanking from "./ChampionshipRanking";
import { TeamScore } from "../../pages/api/getTeamScores";
import Button from "../Shared/Button";

const mergeRankings = (stagesRankingCells: RankingCell[][]) => {
  const mergedRankings: { [name: string]: RankingCell } = {};

  stagesRankingCells
    .filter((stageRankingCells) => stageRankingCells != null)
    .forEach((stageRankingCells) => {
      stageRankingCells.forEach((ranking) => {
        if (mergedRankings[ranking.name]) {
          mergedRankings[ranking.name].score += ranking.score;
        } else {
          mergedRankings[ranking.name] = { ...ranking };
        }
      });
    });
  const mergedRankingsArray: RankingCell[] = [];

  for (let name in mergedRankings) {
    mergedRankingsArray.push(mergedRankings[name]);
  }
  mergedRankingsArray.sort((a, b) => b.score - a.score);

  return mergedRankingsArray;
};

const NewsList = ({ allNews }: { allNews: any[] }) => {
  const [currentPage, setCurrentPage] = useState<number>(1);
  const newsByPage = 6;
  const isLarge = useMedia({ minWidth: 1024 });
  const isSmall = useMedia({ maxWidth: 768 });
  const newsStart = (currentPage - 1) * newsByPage;
  const newsEnd = Math.min(currentPage * newsByPage, allNews.length);
  return (
    <>
      <h1 className="text-center font-scout stretch-condensed text-4xl font-bold uppercase">Últimas Notícias</h1>
      <div>
        {allNews.slice(newsStart, newsEnd).map((news, i) => {
          return <NewsBlock news={news} key={i} size="sm" />;
        })}
      </div>
      <PageSelection
        formFactor={isLarge || isSmall ? "ULTRA-COMPRESSED" : "COMPRESSED"}
        maxPageDisplay={5}
        currentPage={currentPage}
        setPageNumber={(num) => setCurrentPage(num)}
        totalNumberOfPages={Math.ceil(allNews.length / newsByPage)}
      />
    </>
  );
};

type RankingCell = { name: string; imgUrl: string; score: number };

const RankingTableLine: React.FC<{ ranking: RankingCell; position: number }> = ({ position, ranking }) => (
  <tr className="border-b-2 border-gray-200 bg-gray-300 uppercase" key={position}>
    <td className="px-4 py-2 w-6 font-bold font-scout border-r-2 border-gray-200 text-xl bg-gray-500">
      {position + 1}
    </td>
    <td className="p-0 w-10">
      <img className="w-10" src={ranking.imgUrl === "" ? "/Capture.PNG" : ranking.imgUrl}></img>
    </td>
    <td className="px-4 py-2 font-scout border-r-2 border-gray-200 text-lg">{ranking.name}</td>
    <td className="px-4 py-2 w-12 font-scout font-bold text-lg">{ranking.score}</td>
  </tr>
);

const RankingTable: React.FC<{ rankings: RankingCell[] }> = ({ rankings }) => {
  const [showAll, setShowAll] = useState(false);
  const maxNumLines = 15;
  const renderedRankings = showAll ? rankings : rankings.slice(0, maxNumLines);
  return (
    <>
      <table className="table-auto p-4 mb-4 w-full">
        <thead className="bg-black text-white">
          <tr>
            <th className="px-4 py-2 w-6 font-bold font-scout border-r-2 border-gray-200">#</th>
            <th className="p-0 w-10"></th>
            <th className="px-4 py-2 font-scout text-left border-r-2 border-gray-200">Equipe</th>
            <th className="px-4 py-2 w-12 font-scout">Pontos</th>
          </tr>
        </thead>
        <tbody>
          {renderedRankings.map((ranking, i) => (
            <RankingTableLine ranking={ranking} position={i} key={i} />
          ))}
        </tbody>
      </table>
      <div className="flex justify-center">
        <Button onClick={() => setShowAll(!showAll)}>
          Mostrar {showAll ? "menos" : "mais"}
        </Button>
      </div>
    </>
  );
};
interface RankingTabHeaderProps {
  selected: boolean;
  onClick: () => void;
}

const RankingTabHeader: React.FC<RankingTabHeaderProps> = ({ onClick, selected, children }) => (
  <div
    onClick={onClick}
    className={
      "px-2 py-2 cursor-pointer font-scout text-lg font-bold stretch-condensed uppercase flex-grow flex justify-center" +
      (selected ? " bg-blue-500" : " bg-white hover:bg-blue-100")
    }
  >
    {children}
  </div>
);

type RankingTabs = number | "OVERALL";
interface RankingProps {
  championshipIds: (string | null)[];
  getScoreFromRankingPosition: (number) => number;
}
const Ranking: React.FC<RankingProps> = ({ championshipIds, getScoreFromRankingPosition }) => {
  const [selectedTab, setSelectedTab] = useState<RankingTabs>(0);
  let [stageRankings, setStageRankings] = useState<Leaderboard[]>([]);
  let [loadingRankings, setLoadingRankings] = useState<boolean>(false);

  useEffect(() => {
    let leaderboardRequests: Promise<Leaderboard>[] = [];
    championshipIds.forEach((championshipId) => {
      if (championshipId) {
        setLoadingRankings(true);
        leaderboardRequests.push(getLeaderboard(championshipId));
      } else {
        leaderboardRequests.push(Promise.resolve(null));
      }
    });

    Promise.all(leaderboardRequests).then((leaderboards) => {
      setStageRankings(leaderboards);
      setLoadingRankings(false);
    });
  }, [championshipIds]);

  const stagesRankingCells: RankingCell[][] = stageRankings.map((ranking) => {
    return ranking?.ranking.map((team, i) => {
      return { name: team.name, imgUrl: team.imgUrl, score: getScoreFromRankingPosition(i + 1) };
    });
  });

  const comingSoonRankings = (() => {
    let range = [];
    range[31] = 0;
    range.fill(0, 0, 32);
    return range.map((_, i) => {
      return { name: "Em breve...", imgUrl: "/Capture.PNG", score: getScoreFromRankingPosition(i + 1) };
    });
  })();

  const rankingTable = (() => {
    if (loadingRankings) {
      return <div>Loading...</div>;
    }
    switch (selectedTab) {
      case "OVERALL":
        return <RankingTable rankings={mergeRankings(stagesRankingCells)} />;
      default:
        return <RankingTable rankings={stagesRankingCells[selectedTab] ?? comingSoonRankings} />;
    }
  })();

  return (
    <>
      <div className="flex w-full">
        {championshipIds.map((_, i) => (
          <RankingTabHeader onClick={() => setSelectedTab(i)} selected={selectedTab === i} key={i}>
            Liga Six - Edição {i + 1}
          </RankingTabHeader>
        ))}
        <RankingTabHeader onClick={() => setSelectedTab("OVERALL")} selected={selectedTab === "OVERALL"}>
          Ranking Geral
        </RankingTabHeader>
      </div>
      {rankingTable}
    </>
  );
};

interface LeagueTabProps {
  allNews: News[];
  hidden: boolean;
  scoringFunction: (number) => number;
  championshipIds: (string | null)[];
  championshipRankingTitle: string;
  teamScores?: TeamScore[];
}

const LeagueTab: React.FC<LeagueTabProps> = ({
  allNews,
  championshipIds,
  scoringFunction,
  hidden,
  teamScores,
  championshipRankingTitle
}) => {

  return (
    <div className={"flex flex-wrap-reverse container mx-auto pb-10 " + (hidden ? "hidden" : "")}>
      <div className="w-full lg:w-1/2 px-2 md:px-8">
        <NewsList allNews={allNews} />
      </div>
      <div className="w-full lg:w-1/2 px-2 md:px-8">
        <h1 className="text-center font-scout stretch-condensed text-4xl font-bold uppercase">{championshipRankingTitle}</h1>
        <ChampionshipRanking teamScores={teamScores} />
        <h1 className="text-center font-scout stretch-condensed text-4xl font-bold uppercase">RANKING LIGA SIX</h1>
        <Ranking getScoreFromRankingPosition={scoringFunction} championshipIds={championshipIds} />
      </div>
    </div>
  );
};

export default LeagueTab;
