import { TeamScore } from "../../pages/api/getTeamScores";

const ChampionshipRankingTableLine: React.FC<{ teamScore: TeamScore; position: number }> = ({
  teamScore,
  position,
}) => {
  const positionColorClass = position === 10 ? "bg-gray-600" : position > 4 ? "bg-gray-400" : "bg-blue-500 text-white";
  return (
    <tr className="border-b-2 border-gray-200 bg-gray-300 uppercase">
      <td className={`px-2 md:px-4 py-2 w-6 font-bold font-scout border-r-2 border-gray-200 text-xl ${positionColorClass}`}>{position}°</td>
      <td className="p-0 w-10">
        <img className="w-10" src={teamScore.teamLogoUrl} />
      </td>
      <td className="px-2 md:px-4 py-2 font-scout border-r-2 border-gray-200 text-lg">{teamScore.teamName}</td>
      <td className={`px-2 md:px-4 py-2 md:w-12 font-scout font-bold text-lg ${positionColorClass}`}>{teamScore.points}</td>
      <td className="px-2 md:px-4 py-2 md:w-12 font-scout font-bold text-lg">{teamScore.gamesPlayed}</td>
      <td className="px-2 md:px-4 py-2 md:w-12 font-scout font-bold text-lg">{teamScore.mapsDifference}</td>
      <td className="px-2 md:px-4 py-2 md:w-12 font-scout font-bold text-lg">{teamScore.mapsWon}</td>
      <td className="px-2 md:px-4 py-2 md:w-12 font-scout font-bold text-lg">{teamScore.roundsDifference}</td>
      <td className="px-2 md:px-4 py-2 md:w-12 font-scout font-bold text-lg">{teamScore.roundsWon}</td>
    </tr>
  )
};

interface ChampionshipRankingProps {
  teamScores: TeamScore[];
}

const ChampionshipRanking: React.FC<ChampionshipRankingProps> = ({ teamScores }) => (
  <>
    <table className="table-auto p-4 mb-4 w-full">
      <thead className="bg-black text-white">
        <tr>
          <th className="px-2 md:px-4 py-2 w-6 font-bold font-scout border-r-2 border-gray-200">POS</th>
          <th className="p-0 w-10"></th>
          <th className="px-2 md:px-4 py-2 font-scout text-left border-r-2 border-gray-200 uppercase">Times</th>
          <th className="px-2 md:px-4 py-2 w-12 font-scout uppercase">Pts</th>
          <th className="px-2 md:px-4 py-2 w-12 font-scout uppercase">J</th>
          <th className="px-2 md:px-4 py-2 w-12 font-scout uppercase">SM</th>
          <th className="px-2 md:px-4 py-2 w-12 font-scout uppercase">MV</th>
          <th className="px-2 md:px-4 py-2 w-12 font-scout uppercase">SR</th>
          <th className="px-2 md:px-4 py-2 w-12 font-scout uppercase">RV</th>
        </tr>
      </thead>
      <tbody>
        {teamScores ? teamScores.map((score, i) => (
          <ChampionshipRankingTableLine teamScore={score} position={i + 1} key={i} />
        )) : "Carregando..."}
      </tbody>
    </table>
  </>
);

export default ChampionshipRanking;
