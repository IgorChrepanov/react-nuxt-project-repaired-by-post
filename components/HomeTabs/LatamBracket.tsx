import teamLogosConfig from "../../config/team-logos-config.json";
import latamBracketConfig from "../../config/latam-bracket-config.json";

const LatamBracket: React.FC = () => {
  // largura dos retangulos 20 
  // altura dos retangulos 5
  const [firstColumn, secondColumn, thirdColumn, championColumn] = latamBracketConfig;

  const borderColor = "#444";
  const lineColor = borderColor;

  return (
    <svg className="w-full" viewBox="-2.5 -2.5 105 55" style={{ minWidth: "850px" }}>
      {
        (firstColumn as string[]).map(
          (teamName, i) => {
            const yBase = i >= 4 ? i * 8 + 5 : i * 8;
            return (
              <g>
                <rect x="-0.25" y={yBase - 0.25} width="16.5" height="5.5" style={{ fill: borderColor }} />
                <rect x="0" y={yBase} width="16" height="5" style={{ fill: "white" }} />
                <rect x="0" y={yBase} width="5" height="5" style={{ fill: "#EEE" }} />
                <image href={`./team_logos/${teamLogosConfig[teamName]}`} height="4" width="4" x="0.5" y={yBase + 0.5} />
                <text x="5.5" y={yBase + 3} fontSize="0.07em" fontWeight="bold" className="uppercase">{teamName}</text>
                <line x1="16" y1={yBase + 2.5} x2="22" y2={yBase + 2.5} style={{ stroke: lineColor, strokeWidth: 0.15 }} />
                <line x1="22" y1={yBase + 2.5} x2="22" y2={i % 2 === 0 ? yBase + 6.5 : yBase - 1.5} style={{ stroke: lineColor, strokeWidth: 0.15 }} />
              </g>
            )
          }
        )
      }
      {
        (secondColumn as string[]).map((teamName, i) => {
          const yBase = i >= 2 ? i * 8 + 17 : i * 16 + 4;
          const lineSize = i >= 2 ? 6.5 : 10.5;
          return (
            <g>
              {i !== 2 && <line x1="28" y1={yBase + 2.5} x2="22" y2={yBase + 2.5} style={{ stroke: lineColor, strokeWidth: 0.15 }} />}
              <rect x="27.75" y={yBase - 0.25} width="16.5" height="5.5" style={{ fill: borderColor }} />
              <rect x="28" y={yBase} width="16" height="5" style={{ fill: "white" }} />
              <rect x="28" y={yBase} width="5" height="5" style={{ fill: "#EEE" }} />
              <image href={`./team_logos/${teamLogosConfig[teamName]}`} height="4" width="4" x="28.5" y={yBase + 0.5} />
              <text x="33.5" y={yBase + 3} fontSize="0.07em" fontWeight="bold" className="uppercase">{teamName}</text>
              <line x1="44" y1={yBase + 2.5} x2="50" y2={yBase + 2.5} style={{ stroke: lineColor, strokeWidth: 0.15 }} />
              <line x1="50" y1={yBase + 2.5} x2="50" y2={i % 2 === 0 ? yBase + 2.5 + lineSize : yBase + 2.5 - lineSize} style={{ stroke: lineColor, strokeWidth: 0.15 }} />
            </g>
          )
        }
        )
      }
      {
        (thirdColumn as string[]).map((teamName, i) => {
          const yBase = i === 0 ? 12 : 36.75;
          return (
            <g>
              <line x1="56" y1={yBase + 2.5} x2="50" y2={yBase + 2.5} style={{ stroke: lineColor, strokeWidth: 0.15 }} />
              <rect x="55.75" y={yBase - 0.25} width="16.5" height="5.5" style={{ fill: borderColor }} />
              <rect x="56" y={yBase} width="16" height="5" style={{ fill: "white" }} />
              <rect x="56" y={yBase} width="5" height="5" style={{ fill: "#EEE" }} />
              <image href={`./team_logos/${teamLogosConfig[teamName]}`} height="4" width="4" x="56.5" y={yBase + 0.5} />
              <text x="61.5" y={yBase + 3} fontSize="0.07em" fontWeight="bold" className="uppercase">{teamName}</text>
              <line x1="72" y1={yBase + 2.5} x2="78" y2={yBase + 2.5} style={{ stroke: lineColor, strokeWidth: 0.15 }} />
              <line x1="78" y1={yBase + 2.5} x2="78" y2={i % 2 === 0 ? yBase + 14.875 : yBase - 9.875} style={{ stroke: lineColor, strokeWidth: 0.15 }} />
            </g>
          )
        }
        )
      }
      <g>
        <line x1="84" y1="26.875" x2="78" y2="26.875" style={{ stroke: lineColor, strokeWidth: 0.15 }} />
        <rect x="83.75" y="24.125" width="16.5" height="5.5" style={{ fill: borderColor }} />
        <rect x="84" y="24.375" width="16" height="5" style={{ fill: "white" }} />
        <rect x="84" y="24.375" width="5" height="5" style={{ fill: "#EEE" }} />
        <image href={`./team_logos/${teamLogosConfig[championColumn as string]}`} height="4" width="4" x="84.5" y={24.875} />
        <text x="89.5" y={27.375} fontSize="0.07em" fontWeight="bold" className="uppercase">{championColumn}</text>
      </g>
    </svg>
  );
};

export default LatamBracket;
