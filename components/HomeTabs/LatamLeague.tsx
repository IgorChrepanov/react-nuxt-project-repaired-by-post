import MainNews from "../News/MainNews";
import LatamBracket from "./LatamBracket";

const LatamLeague = ({ news }) => {
  return (
    <div className="pt-10">
      <MainNews news={news} />
      <div className="w-full py-10 mt-10 bg-cover" style={{ backgroundImage: "url(./latam-bracket-bg.png)" }}>
        <div className="container mx-auto overflow-x-auto">
          <LatamBracket />
        </div>
      </div>
      <div
        style={{ backgroundImage: "url(./partners-bg.PNG)", backgroundSize: "cover", backgroundRepeat: "no-repeat" }}
        className="p-4 md:p-8 mt-10"
      >
        <h1 className="text-white font-scout font-bold stretch-condensed uppercase text-center text-2xl md:text-4xl lg:text-6xl mt-0">
          Patrocinadores
        </h1>
        <div className="flex justify-center container mx-auto">
          <img src="./predator-logo.svg" className="w-1/4 m-2 sm:m-8 h-auto" />
          {/* <img src="./bancodobrasil-white.svg" className="w-1/4 m-2 sm:m-8 h-auto" />} */}
          <img src="./corsair-logo.svg" className="w-1/4 m-3 sm:m-12 h-auto" />
        </div>
      </div>
    </div>
  );
};

export default LatamLeague;
