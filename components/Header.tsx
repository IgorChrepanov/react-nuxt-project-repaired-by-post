import React, { useRef } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import useDropdownOnHover from "../lib/hooks/useDropdownOnHover";
import { RiArrowDownSLine } from "react-icons/ri";

const MenuLink: React.FC<{ href: string }> = (props) => {
  const router = useRouter();
  return (
    <Link href={props.href}>
      <a
        style={{
          backgroundImage: "linear-gradient(to bottom, #007ace 50%, white 50%)",
          transition: "all .4s cubic-bezier(.23,1,.32,1)"
        }}
        className={
          "flex items-center bg-left-bottom hover:bg-left-top h-full mx-0 px-4 md:px-6 md:mx-2 font-scout bg-100-200" +
          (router.pathname.startsWith(props.href) ? " bg-left-top" : "")
        }
      >
        {props.children}
      </a>
    </Link>
  );
};

const MenuDropdown: React.FC<{ title: string }> = (props) => {
  const [dropdownContainerRef, isDropdownOpen, toggleDropdown] = useDropdownOnHover<
    HTMLDivElement
  >();

  return (
    <div ref={dropdownContainerRef} className="flex flex-col mx-0 md:mx-2 h-full font-scout">
      <div
        role="button"
        onClick={toggleDropdown}
        className={`flex items-center h-full px-4 md:px-6 ${isDropdownOpen ? "bg-blue-500" : "bg-white"}`}
      >
        {`${props.title}  `}<RiArrowDownSLine size={32} className={`transform duration-200 transition-transform ease-in-out ${isDropdownOpen ? "rotate-180" : ""}`} />
      </div>
      <div
        className={`flex flex-col absolute bg-blue-500 ${!isDropdownOpen ? "hidden" : ""}`}
        style={{ top: "5rem" }}
      >
        {props.children}
      </div>
    </div>
  );
};

const Header: React.FC = () => {
  return (
    <div className="h-20">
      <div className="flex fixed items-center justify-start w-full h-20 bg-white text-black text-base sm:text-xl md:text-2xl font-bold uppercase z-20">
        <Link href="/">
          <a>
            <img src="/Artboard12.png" className="h-16 mx-4" />
          </a>
        </Link>
        <MenuLink href="/news">Notícias</MenuLink>
        {/* {<MenuLink href="/about">About</MenuLink>} */}
        <MenuLink href="/events">Eventos</MenuLink>
        <MenuDropdown title="Rulebook">
          <a className="p-4 md:p-6 hover:text-white"
            style={{ transition: "color .4s cubic-bezier(.23,1,.32,1)" }}
            href="/R6C_LATAMRulebook.pdf" target="_blank">
            LATAM Rulebook
          </a>
          <a className="p-4 md:p-6 hover:text-white"
            style={{ transition: "color .4s cubic-bezier(.23,1,.32,1)" }}
            href="/R6Circuit_GlobalRulebook.pdf" target="_blank">
            GLOBAL Rulebook
          </a>
          <a className="p-4 md:p-6 hover:text-white"
            style={{ transition: "color .4s cubic-bezier(.23,1,.32,1)" }}
            href="/R6Esports_CoC.pdf" target="_blank">
            Code Of Conduct
          </a>
        </MenuDropdown>
      </div>
    </div>
  );
};

export default Header;
