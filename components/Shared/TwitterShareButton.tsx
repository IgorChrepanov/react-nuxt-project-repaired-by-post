import React from "react";

interface TwitterShareButtonProps {
  tweetText?: string;
}

const TwitterShareButton: React.FC<TwitterShareButtonProps> = (props) => (
  <div
    className="mx-2 cursor-pointer"
    onClick={() => {
      const twitterShareUrl = `https://twitter.com/intent/tweet?text=${props.tweetText ?? ""}&url=${encodeURIComponent(
        window.location.href
      )}`;

      window.open(
        twitterShareUrl,
        "targetWindow",
        `width=${500},
         height=${500}`
      );
    }}
  >
    <img src="/twitter-logo.svg" className="h-20 w-20" />
  </div>
);

export default TwitterShareButton;
