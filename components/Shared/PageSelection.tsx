import React from "react";
import Button from "./Button";

interface PageSelectionProps {
    currentPage: number;
    setPageNumber: (number) => void;
    totalNumberOfPages: number;
    formFactor: "EXTENDED" | "COMPRESSED" | "ULTRA-COMPRESSED";
    maxPageDisplay: number;
}
const PageSelection: React.FC<PageSelectionProps> = ({ currentPage, setPageNumber, totalNumberOfPages, formFactor, maxPageDisplay }) => {
    const pageDisplayStart = maxPageDisplay < totalNumberOfPages ?
        Math.min(totalNumberOfPages - maxPageDisplay + 1, Math.max(1, currentPage - Math.floor(maxPageDisplay / 2))) :
        1;
    const pageDisplayEnd = Math.min(totalNumberOfPages, pageDisplayStart + maxPageDisplay - 1);

    const pageButtons = [];
    for (let i = pageDisplayStart; i <= pageDisplayEnd; i++) {
        let xMargin = "mx-2";
        if (i === pageDisplayStart) xMargin = "mr-2";
        else if (i === pageDisplayEnd) xMargin = "ml-2";
        pageButtons.push(
            (<Button key={i} outline customClassName={xMargin} onClick={() => setPageNumber(i)} active={currentPage === i}>
                {i}
            </Button>)
        )
    }

    return (
        <>
            <div className={"flex container mx-auto justify-between " + (formFactor === "ULTRA-COMPRESSED" ? "flex-wrap" : "")}>
                <div className={"flex " + (formFactor === "ULTRA-COMPRESSED" ? "w-1/2" : "")}>
                    <Button outline customClassName="mr-2" onClick={() => setPageNumber(1)} disabled={currentPage === 1}>
                        {formFactor === "EXTENDED" ? '< Primeira' : '<<'}
                    </Button>
                    <Button outline customClassName="mx-2" onClick={() => setPageNumber(currentPage - 1)} disabled={currentPage === 1}>
                        {formFactor === "EXTENDED" ? '< Anterior' : '<'}
                    </Button>
                </div>
                <div className={"flex " + (formFactor === "ULTRA-COMPRESSED" ? "order-first w-full mb-2 justify-between" : "")}>
                    {pageButtons}
                </div>
                <div className={"flex " + (formFactor === "ULTRA-COMPRESSED" ? "w-1/2 justify-end" : "")}>
                    <Button outline customClassName="mx-2" onClick={() => setPageNumber(currentPage + 1)} disabled={currentPage === totalNumberOfPages}>
                        {formFactor === "EXTENDED" ? 'Avançar >' : '>'}
                    </Button>
                    <Button outline customClassName="ml-2" onClick={() => setPageNumber(totalNumberOfPages)} disabled={currentPage === totalNumberOfPages}>
                        {formFactor === "EXTENDED" ? 'Última >' : '>>'}
                    </Button>
                </div>
            </div>
        </>
    )
}

export default PageSelection;