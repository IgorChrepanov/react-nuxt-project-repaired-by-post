import React from "react";

const FacebookShareButton: React.FC = () => (
  <div
    className="flex items-center mx-2 cursor-pointer"
    onClick={() => {
      const facebookShareUrl =
        "https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(window.location.href);
      window.open(
        facebookShareUrl,
        "targetWindow",
        `width=${500},
         height=${500}`
      );
    }}
  >
    <img src="/facebook-logo.png" className="h-12 w-12" />
  </div>
);

export default FacebookShareButton;
