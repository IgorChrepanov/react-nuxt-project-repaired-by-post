import React from "react";

interface ButtonProps {
  customClassName?: string;
  outline?: boolean;
  active?: boolean;
  disabled?: boolean;
  onClick?: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
}

const Button: React.FC<ButtonProps> = (props) => {
  const buttonOpacity = props.disabled ? "opacity-25" : "";
  const buttonColorStyle = props.active ?
    (props.outline ? "text-white border-blue-500 border-2 bg-blue-400 " : "text-white hover:bg-orange-500 bg-blue-500 ") :
    (props.outline ? "text-blue-500 border-blue-500 border-2 bg-white hover:bg-blue-400 " : "text-white hover:bg-orange-500 bg-blue-500 ")
  // TODO: add active styles for non-outline active button

  return (
    <div
      role="button"
      onClick={!props.disabled ? props.onClick : null}
      className={
        `${props.customClassName} ${buttonOpacity} ${buttonColorStyle} ` +
        "inline-flex items-center content-center justify-center font-bold uppercase px-6 h-12 cursor-pointer font-scout "
      }
    >
      {props.children}
    </div>
  );
};

export default Button;
