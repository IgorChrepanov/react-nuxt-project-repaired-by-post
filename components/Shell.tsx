import Head from "next/head";
import Header from "./Header";
import Footer from "./Footer";

export default (Page) => {
  return (props) => (
    <div>
      <Head>
        <title>Rainbow Six Siege - Esports | LATAM</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="icon" href="/favicon.ico" />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,500;0,700;1,500;1,700&display=swap" rel="stylesheet"></link>
      </Head>
      <main>
        <Header />
        <Page {...props} />
      </main>

      <footer>
        <Footer />
      </footer>
    </div>
  );
};
