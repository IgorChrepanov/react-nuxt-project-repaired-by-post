import React from "react";
import { News } from "../../lib/dal/news";
import Link from "next/link";
import Button from "../Shared/Button";
import useClampText from "../../lib/hooks/useClampText";

const MainNews: React.FC<{ news: News }> = ({ news }) => {
  const excerptRef = useClampText(news.excerpt);
  const titleRef = useClampText(news.title);

  const newsDate = new Date(news.date);

  return (
    <div className="flex justify-center w-full">
      <div className="container mx-auto">
        <div className="flex w-full bg-white">
          <div className="w-1/2 py-5 lg:py-10 px-4 lg:px-8">
            <h4 className="font-bold text-blue-500 mb-2 text-lg">
              {("0" + newsDate.getDate()).slice(-2)} {("0" + (newsDate.getMonth() + 1)).slice(-2)}{" "}
              {newsDate.getFullYear()}
            </h4>
            <Link href="news/[slug]" as={"/news/" + news.slug}>
              <a>
                <h1
                  ref={titleRef}
                  className={`overflow-hidden mb-3 max-h-24 lg:max-h-48
                    font-bold text-base xs:text-xl sm:text-2xl lg:text-4xl uppercase font-scout stretch-condensed leading-tight
                    hover:underline`}
                >
                  {news.title}
                </h1>
              </a>
            </Link>
            <p className="overflow-hidden h-16 mb-3 font-scout text-xl" ref={excerptRef}>
              {news.excerpt}
            </p>
            <Link href="news/[slug]" as={"/news/" + news.slug}>
              <a>
                <Button>
                  <span className="text-2xl">
                    LEIA MAIS
                  </span>
                </Button>
              </a>
            </Link>
          </div>
          <div className="w-3/5">
            <img src={news.img} className="object-cover h-full w-full" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainNews;
