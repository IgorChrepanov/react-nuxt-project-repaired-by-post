import React from "react";
import Button from "../Shared/Button";
const NewsFilter: React.FC = () => {
  return (
    <div className="w-full bg-gray-100 py-6 px-4">
      <div className="flex justify-between container mx-auto">
        <div className="flex items-center">
          <h1 className="uppercase mr-6 font-scout">Filtrar:</h1>
          <select className="appearance-none border-2 border-blue-500 focus:border-blue-800 h-12 px-4">
            <option>Todas notícias</option>
            <option>Brasil</option>
            <option>Mexico</option>
            <option>Região Sul</option>
          </select>
        </div>
        <div>
          <Button>Reiniciar filtros</Button>
        </div>
      </div>
    </div>
  );
};

export default NewsFilter;
