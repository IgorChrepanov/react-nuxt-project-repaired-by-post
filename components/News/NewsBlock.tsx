import React from "react";
import { News } from "../../lib/dal/news";
import Link from "next/link";
import useClampText from "../../lib/hooks/useClampText";

type Size = "lg" | "sm";

const NewsBlock: React.FC<{ news: News; size: Size }> = ({ news, size }) => {
  const excerptRef = useClampText(news.excerpt);
  const titleRef = useClampText(news.title);

  const newsDate = new Date(news.date);

  const titleSizeStyle =
    size === "lg"
      ? " text-lg xs:text-xl sm:text-2xl lg:text-3xl mb-3 max-h-12 lg:max-h-24 "
      : " xs:text-lg sm:text-xl lg:text-2xl mb-2 max-h-6 lg:max-h-12 ";

  const excerptSizeStyle = size === "lg" ? " max-h-20 mb-3 " : " max-h-20 mb-2 ";

  const containerSizeStyle = size === "lg" ? " py-3 lg:py-6 px-4 lg:px-8 " : " py-2 lg:py-4 px-2 lg:px-6 ";

  return (
    <div className="flex w-full bg-white mb-8">
      <div className="w-1/3">
        <img src={news.img} className="object-cover h-full w-full" />
      </div>
      <div className={"w-2/3" + containerSizeStyle}>
        <h4 className="font-bold text-blue-500 mb-2 text-lg font-scout">
          {("0" + newsDate.getDate()).slice(-2)} {("0" + (newsDate.getMonth() + 1)).slice(-2)} {newsDate.getFullYear()}
        </h4>
        <Link href="news/[slug]" as={"/news/" + news.slug}>
          <a>
            <h2
              ref={titleRef}
              className={
                "overflow-hidden font-bold uppercase" +
                titleSizeStyle +
                "hover:underline font-scout stretch-condensed leading-tight"
              }
            >
              {news.title}
            </h2>
          </a>
        </Link>
        <p className={"overflow-hidden font-scout text-lg" + excerptSizeStyle} ref={excerptRef}>
          {news.excerpt}
        </p>
      </div>
    </div>
  );
};

export default NewsBlock;
