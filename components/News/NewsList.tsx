import React from "react";
import { News } from "../../lib/dal/news";
import NewsBlock from "./NewsBlock";

const NewsList: React.FC<{ newsList: News[] }> = ({ newsList }) => {
  return (
    <div className="w-full">
      <div className="container mx-auto">
        {newsList.map((news, i) => (
          <NewsBlock news={news} key={i} size="lg" />
        ))}
      </div>
    </div>
  );
};

export default NewsList;
