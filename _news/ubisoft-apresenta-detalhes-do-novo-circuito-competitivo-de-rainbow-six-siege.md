---
title: 'UBISOFT APRESENTA DETALHES DO NOVO CIRCUITO COMPETITIVO DE RAINBOW SIX SIEGE'
excerpt: 'A espera dos fãs de Rainbow Six Siege acabou. A Ubisoft apresentou nesta quarta-feira (20) todos os detalhes do novo circuito competitivo da modalidade, que ganhou calendário completo e competições reformuladas.'
img: '/news1.jpg'
date: '2020-05-20'
---

##  espera dos fãs de Rainbow Six Siege acabou. A Ubisoft apresentou nesta quarta-feira (20) todos os detalhes do novo circuito competitivo da modalidade, que ganhou calendário completo e competições reformuladas.

A empresa iniciou seu programa de regionalização na América Latina, América do Norte, Europa e Ásia-Pacífico. Desta forma, cada uma das quatro regiões decidirá sobre as particularidades de seus campeonatos, o que permitirá um foco maior nas características locais e o desenvolvimento de um circuito competitivo verdadeiramente global dentro do cenário dos esports.

Sendo assim, o ano de 2020 terá um calendário de transição. Com o término da Rainbow Six Pro League, os quatro programas regionais acontecerão em paralelo. As equipes envolvidas nestes torneios se classificarão com base em um sistema de pontos mundial, que leva em conta o desempenho nos campeonatos regionais e nos Majors, o que resultará em uma emocionante corrida global em direção ao Six Invitational, o auge do ano competitivo.

Na América Latina, o cenário competitivo ganhará ainda mais destaque, além de novos campeonatos. O Brasileirão Rainbow Six Siege 2020, torneio de maior expressão da modalidade no país, terá início em julho e já possui seus 10 times confirmados.

Além dele, o público também terá a chance de ver os melhores times da região latino-americana se enfrentarem na Copa Elite Six da América, um torneio continental que vai reunir equipes de diversos países.

Brasileirão Rainbow Six Siege 2020

O BR6 está de cara nova. A quarto ano da competição já tem os seus 10 times participantes confirmados e será caminho para os Six Majors que acontecerão ao longo do ano.

As equipes que disputarão o Brasileirão Rainbow Six Siege 2020 são: Team Liquid, Faze Clan, Ninjas in Pyjamas, MIBR, Black Dragons, INTZ e-Sports, Team oNe, FURIA Esports, Santos e-Sports e W7M Gaming.

O campeonato terá dois turnos em 2020. O primeiro será disputado entre julho e agosto, enquanto o segundo em setembro e outubro. Os jogos acontecerão nas quintas, sábados e domingos, a partir das 13h, com transmissão ao vivo dos canais da Rainbow Six Esports Brasil na Twitch e no YouTube.

[R6S] [News] Brasil Competitve Circuit - calendar

Serão três partidas por dia no formato MD2 (melhor de dois mapas). Desta forma, vitórias por dois mapas a zero valem três pontos na tabela de classificação e vitórias por um mapa a zero, dois pontos. Empates por um a um ou zero a zero valem um ponto.

Em novembro, no fim da fase regular, os quatro times que tiverem somado mais pontos nos dois turnos da competição estarão classificados para as finais. Enquanto isso, o último colocado da temporada regular buscará a chance de se manter na elite do Rainbow Six Siege brasileiro em partida contra o campeão da Série B.

[R6S] [News] Brasil Competitve Circuit - setup

A expectativa é que as finais do BR6 sejam disputadas em uma arena para milhares de torcedores. O evento será dividido em dois dias: no primeiro, as equipes se enfrentam pelas semifinais no formato MD3 (melhor de três mapas); no segundo, a grande final acontecerá em um confronto MD5 (melhor de cinco mapas).

Uma das grandes novidades do Brasileirão Rainbow Six Siege 2020 será a possibilidade de classificação para o Major da modalidade. O BR6 servirá de caminho para a Copa Elite Six, um campeonato continental que reunirá os melhores times dos torneios latino-americanos em uma disputa classificatória para o Six Major de cada temporada.

“Este será o maior ano do Rainbow Six Siege em relação a eventos, participantes e patrocinadores. 2020 e 2021 ficarão marcados como o período de crescimento e consolidação da modalidade, para chegar ao mesmo nível de outros grandes esportes tradicionais. A expectativa é que este novo calendário dê oportunidades para os cenários regionais se desenvolverem e ficarem cada vez mais fortes, além de aumentar a visibilidade dos talentos de cada local. Para o Brasil, nosso objetivo a longo prazo continua o mesmo: fazer com que o país permaneça como uma das maiores potências do cenário competitivo mundial de Rainbow Six Siege”, afirma Bertrand Chaverot, diretor geral da Ubisoft para América Latina.

Novo Local e Plano de Contingência para o COVID-19

O Brasileirão Rainbow Six Siege 2020 será disputado na Max Arena, que conta com área de mais de 6200m², e está localizada no bairro da Mooca, em São Paulo. Devido à pandemia causada pela COVID-19, a Ubisoft Brasil ainda estuda o melhor formato para realizar os jogos da competição de maneira a garantir a saúde e o bem-estar dos jogadores, torcedores, parceiros e equipes. Sendo assim, ainda não há definição se as partidas serão presenciais logo de início.

A organização do campeonato trabalha com três possibilidades. A primeira consiste na realização do torneio com times, staff e equipe de transmissão no local. A segunda opção não inclui a presença dos atletas na Max Arena. Por último, caso seja necessário, a competição será realizada de forma completamente remota e online.

“Nossa equipe de esports da Ubisoft tem monitorado a situação de perto e prestado atenção às recomendações das autoridades relevantes. Este é um período sem precedentes nos esportes tradicionais e eletrônicos, e todos estão se esforçando para fazer o máximo para assegurar a saúde e o bem-estar de suas comunidades”, ressalta Marcio Canosa, Diretor de Esports da Ubisoft para a América Latina.

Região latino-americana

Na América Latina, o cenário estará mais disputado do que nunca. Além do Brasileirão Rainbow Six Siege, o continente também contará com uma nova temporada do Campeonato Mexicano, além do Campeonato Sul-Americano, que terá a participação de times de Chile, Argentina, Peru e Colômbia. Todos eles se encontrarão na Copa Elite Six, torneio presencial que dará vagas diretas aos Majors.

Copa Elite Six

A Copa Elite Six da América será formada por oito times ao todo em cada temporada: as cinco melhores equipes do respectivo turno do Brasileirão, os dois melhores times do Campeonato Mexicano e o primeiro colocado do Campeonato Sul-Americano. Serão duas temporadas da Copa Elite Six em 2020, uma em agosto e a outra em outubro, sempre precedendo os Majors.

“Para a nossa comunidade, a Copa Elite Six representa um retorno às raízes do competitivo de R6. Foi a nossa primeira competição profissional, que reuniu milhares de torcedores em três finais presenciais no Rio de Janeiro e São Paulo em 2016, e consagrou Team United, Santos Dexterity e INTZ como os primeiros campeões latino-americanos de R6 no seu primeiro ano. Estamos felizes em trazer a Elite Six de volta, desta vez ainda maior com a inclusão das equipes mexicanas e o formato presencial”, afirma Marcio Canosa.

São quatro dias de disputa por temporada, com as oito organizações divididas em dois grupos e com todos do grupo se enfrentando em partidas em Melhor de 3 Mapas (MD3). Os dois melhores classificados de cada grupo avançam para as semifinais e estão automaticamente qualificados para o Six Major. As semifinais e a Grande Final também são disputadas em MD3 e, ao fim do evento, teremos a coroação do campeão com o título de melhor time da modalidade na América Latina da temporada.

[R6S] [News] Brasil Competitve Circuit - elite-six

Para mais informações sobre o cenário competitivo global de Rainbow Six Siege, assista ao vídeo disponível neste link.

Série B e Liga Six

O caminho para a elite do R6 nacional também está de cara nova. A partir de agora, os jogadores que quiserem alcançar o topo do cenário brasileiro terão três maneiras de fazê-lo. A primeira será por meio da Liga Six e da Série B.

A Liga Six já é conhecida pelos fãs da modalidade. Reformulada, a competição será a porta de entrada para o cenário competitivo. Neste ano, contará com três torneios mensais em maio, junho e julho. Cada uma delas terá pontuação baseada no sistema suíço e será aberta a todas as equipes interessadas com jogadores acima dos 16 anos. Ao final dos três torneios, os oito times que somarem mais pontos estarão classificados para a Série B do Brasileirão Rainbow Six 2020.

[R6S] [News] Brasil Competitve Circuit - series b

Já a Série B será disputada em turno único, em que todos os times se enfrentam. Os quatro melhores se classificarão para a fase final do campeonato. Após duas semifinais e a grande decisão, a equipe campeã enfrentará o último colocado da série A do BR6 2020 na “Partida de Promoção” que está marcada para o dia 22 de novembro e que decidirá uma vaga para a série A do BR6 2021.

As inscrições para participar da Liga Six podem ser feitas neste link.

Draft e R6 Academy

As outras duas maneiras de chegar à elite do Rainbow Six no Brasil serão por meio da R6 Academy e do Draft. As novidades serão oportunidades para novos talentos conquistarem seus lugares ao sol e representarem as maiores organizações do cenário nacional.

A R6 Academy é o projeto que tem como objetivo o desenvolvimento da base de atletas e a descoberta de novos talentos para o cenário. Desta maneira, as equipes que disputam a série A do Brasileirão poderão contar com suas as próprias line-ups Academy. Os times Academy, por sua vez, estarão aptos a disputar a Liga Six e a Série B, mas não podendo ascender à divisão principal do torneio. Durante as janelas de transferências, os jovens jogadores poderão ser promovidos ao time principal da sua respectiva organização ou negociados com outras organizações.

O Draft, por sua vez, usará um sistema parecido com o da NBA, a liga norte-americana de basquete. O evento dará a oportunidade para equipes de R6 selecionarem promessas da modalidade. Ele ocorrerá em dezembro e janeiro e será a oportunidade para jogadores que não fazem parte de qualquer equipe - seja a line principal ou da R6 Academy - de serem escolhidos pelas dez organizações do Brasileirão. Para isso, os interessados deverão preencher um formulário de inscrição e não possuírem contrato válido com nenhuma organização.

“O sistema de draft vai oferecer a oportunidade para que novas promessas, ou talentos ainda não reconhecidos, encontrem o seu espaço como atletas de R6. Os jogadores selecionados terão ao menos um ano de experiência dentro de uma organização profissional, com toda a infraestrutura necessária para o desenvolvimento de sua carreira no cenário competitivo de Rainbow Six Siege”, reforça Marcio Canosa.

A cerimônia acontecerá em uma noite de gala que está marcada para janeiro. Os dez escolhidos serão anunciados pelas equipes em duas rodadas, em que cada organização pode selecionar um jogador. Os seis primeiros picks estão reservados para times que não se classificaram para os playoffs do Brasileirão. A posição de cada um será definida em sorteio com probabilidades diferentes de acordo com a respectiva colocação final no BR6. O processo se repetirá na segunda rodada, apenas com os quatro melhor classificados do torneio nacional na edição anterior.

Novo calendário

A partir de 2021, o calendário passará a ser completo, com três turnos do Brasileirão, disputados em março-abril, maio-junho e setembro-outubro; três Copas Elite Six, em abril, julho e outubro, e três edições do Six Major, em maio, agosto e novembro, passando por diversas cidades ao redor do mundo, além do Six Invitational, que seguirá acontecendo tradicionalmente em fevereiro.

[R6S] [News] Brasil Competitve Circuit - competive-calendar

Mais de 70 horas de transmissões semanais

A partir de julho, os fãs de Rainbow Six Siege poderão acompanhar todos os seus jogadores preferidos nos canais oficiais do Rainbow Six Esports Brasil na Twitch e no YouTube, que vão transmitir com narração em português mais de 70 horas semanais de competições espalhadas pelas regiões que abrangem o cenário competitivo de R6.

Além dos três dias de disputas do Brasileirão 2020, o canal também vai transmitir todas as emoções da Liga Europeia e da Liga Americana nas segundas e quartas-feiras. Nas quintas e sextas-feiras, será a vez da Liga Sul-Americana. Para encerrar a semana, os jogos da Liga Mexicana acontecem aos sábados e domingos, logo após o BR6.

[R6S] [News] Brasil Competitve Circuit - agenda

Ascensão: uma série Rainbow Six Siege

Em junho, a Ubisoft Brasil lança a série documental ‘Ascensão’, focada na trajetória de jogadores como Nesk (Team Liquid), Lukid (Team oNe) e Muzi (Ninjas in Pyjamas). A série é dirigida pelo jornalista e apresentador Leo Bianchi e cada episódio contará a história de um jogador profissional de R6 com detalhes da sua trajetória rumo ao cenário competitivo da modalidade no país, com entrevistas com familiares, amigos e pessoas próximas. Para assistir ao trailer da série clique aqui.

[R6S] [News] Brasil Competitve Circuit - lineup

Sobre Tom Clancy's Rainbow Six Siege

Inspirado em organizações antiterroristas do mundo real, o Tom Clancy's Rainbow Six Siege coloca seus jogadores no meio de confrontos letais frente a frente. Pela primeira vez em um jogo Tom Clancy's Rainbow Six, jogadores vão invadir cercos, um novo estilo de invasão em que inimigos têm os meios para transformar seus ambientes em fortalezas modernas enquanto os times Rainbow Six lideram uma invasão para conquistar a posição inimiga. Tom Clancy's Rainbow Six Siege dá aos jogadores controle sem precedentes sobre a habilidade de fortificar a sua posição reforçando paredes e pisos, usando arame farpado e reforços implantáveis, colocando minas, ou invadir a posição inimiga usando drones de observação, cargas explosivas, rapel, entre outros. O ritmo acelerado e a singularidade de cada cerco estabelecem um novo padrão para tiroteios intensos, jogabilidade estratégica e jogos competitivos.

Sobre a Ubisoft

A Ubisoft é uma empresa líder na criação, publicação e distribuição de entretenimento e serviços interativos, com um rico portfólio de marcas de renome mundial, incluindo Assassin's Creed, Far Cry, For Honor, Just Dance, Watch Dogs e a série de jogos Tom Clancy incluindo Ghost Recon, Rainbow Six e The Division. As equipes da rede mundial de estúdios e escritórios de negócios da Ubisoft estão comprometidas em oferecer experiências de jogos originais e memoráveis em todas as plataformas populares, incluindo consoles, telefones celulares, tablets e PCs. Para o ano fiscal de 2018-19, a Ubisoft gerou receitas líquidas de € 2.029 milhões. Para saber mais, visite www.ubisoftgroup.com.