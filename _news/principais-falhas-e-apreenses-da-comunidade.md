---
title: 'PRINCIPAIS FALHAS E APREENSÕES DA COMUNIDADE'
excerpt: 'ATUALIZAÇÃO: HACKING, CHEATING E PARALISAÇÃO DO LOBBY
Dando seguimento ao nosso artigo ""Principais falhas"" de abril, gostaríamos de atualizá-los quanto às falhas de hacking, cheating e paralisação do lobby'
img: '/news5.JPG'
date: '2020-05-25'
---

## Lorem Ipsum

Atualizado em: 27 de maio de 2020

ATUALIZAÇÃO: HACKING, CHEATING E PARALISAÇÃO DO LOBBY
Dando seguimento ao nosso artigo ""Principais falhas"" de abril, gostaríamos de atualizá-los quanto às falhas de hacking, cheating e paralisação do lobby. Vamos examinar algumas medidas tomadas que resultaram em sucesso e outras nas quais ainda trabalhamos, além de enfatizar nossa atenção contínua em cheating e hacking como questões prioritárias.

PARALISAÇÃO DO LOBBY
Paralisação do lobby: Ocorre quando nenhum ícone de conectividade aparece no menu ou ao carregar um rodada.

Atualização: Depois de nosso artigo ""Principais falhas"" de abril, fizemos uma atualização no jogo que nos permitiria reunir mais dados para identificar corretamente os infratores. Usamos esse material para implementar um recurso de remoção automática dos indivíduos que atrasam o jogo com carregamentos extremamente demorados. Jogadores removidos receberão uma penalidade por abandono e ficarão de fora do modo Por Colocação/Sem Colocação até o fim da partida. Monitoramos há algum tempo as remoções automáticas nos servidores live e observamos grande sucesso na detecção e expulsão de usuários que paralisam lobbies para prejudicar outros jogadores. Vamos continuar a coletar dados desses infratores nos consoles antes de finalizarmos o planejamento da ativação do recurso de expulsão automática também nessas plataformas.

ATAQUE DOS NOS CONSOLES
Ataques DoS: Diferem da paralisação do lobby, já que normalmente ocorrem durante o gameplay e os ícones de conectividade aparecem.

Atualização: Observamos um pouco de tráfego e atividade de rede incomuns para usuários de consoles e suspeitamos que alguns casos de paralização do lobby na plataforma podem estar ligados a ataques DoS. Estamos investigando a questão e traremos novidades em breve.

HACKING/CHEATING
Atualização: Em nossa última atualização, reenfatizamos que não toleramos cheating in-game e que estávamos preparando uma série de estratégias a curto e longo prazo para tratarmos esse tipo de frustração. Embora saibamos que este seja um problema contínuo, acreditamos que as modificações futuras ajudarão a mitigar algumas adversidades enquanto trabalhamos em soluções mais sólidas de longo prazo.

Banimentos offline (Data prevista: fim de maio): Passaremos a banir jogadores offline. Contas previamente penalizadas eram banidas após logarem novamente em Siege. Agora, as contas serão banidas mesmo estando offline. A medida trará maior clareza no feedback do banimento e melhorará a visibilidade das contas banidas.
Requisitos de Nível de Acesso aumentados para Por Colocação (Previsão: Y5S2): Em Y5S2, aumentaremos as exigências de nível de acesso para partidas Por Colocação. Visamos elevar as barreiras para o acesso de contas ilegítimas nesse modo para promover um melhor ambiente.
Requisitos da colocação Campeão aumentados (Previsão: Y5S2): Em Y5S2, aumentaremos os requisitos de elegibilidade para a colocação Campeão. Além de precisarem de pelo menos 5000 MMR, os jogadores deverão ter jogado no mínimo 100 partidas Por Colocação. Visamos elevar a barreira para o acesso de contas ilegítimas e manter um placar de Campeões limpo.
Nível máximo de XP em PvE: (Status: Ativo) Já estabelecemos um valor máximo de XP em PvE para aumentarmos as barreiras para o acesso de contas smurfs em Por Colocação.
Atualizado em: 23 de abril de 2020

Atualizaremos com frequência a lista a seguir para detalhar o status de certos assuntos urgentes de nossa comunidade. Ressaltamos que esta não é uma lista exaustiva e, portanto, deve ser usada somente como referência para que se saiba onde está nosso foco.



HACKING, CHEATING, PARALISAÇÃO DO LOBBY
Observamos recentemente um aumento nas menções de cheating e nas denúncias dos jogadores. Enfatizamos que não toleramos cheating no jogo e que estamos trabalhando em diversas estratégias para lidar com as frustrações causadas e para garantir a integridade competitiva de Rainbow Six Siege. Compartilhamos abaixo algumas das estratégias que estão sendo trabalhadas a curto prazo.

PARALISAÇÃO DO LOBBY
Descrição: Percebemos um aumento na incidência de paralisadores de lobby que buscam interferir no jogo a seu favor. Lançaremos atualizações que nos permitirão monitorar e rastrear esses infratores com maior eficácia.

Objetivo: As atualizações nos possibilitarão reunir dados necessários para identificar os responsáveis pelas paralizações. Em atualizações futuras, usaremos esses dados para detectar e expulsar paralisadores de lobby automaticamente.

Status: No momento, este é um processo contínuo.

DADOS ATUALIZADOS AUTOMATICAMENTE NO BATTLEYE
Descrição: Devido ao aumento nas menções e denúncias por cheating, reforçaremos a ponte entre R6 e BattlEye.

Objetivo: Fortalecer nossa parceria com o BattlEye implementando processos automatizados e reforçando o compartilhamento de dados para reduzir o ciclo do feedback dos jogadores banidos.

Status: Contínuo.

AUMENTO NOS REQUISITOS DO MODO POR COLOCAÇÃO E NO RANK DE CAMPEÃO
Descrição: Maiores requisitos no modo Por Colocação e no rank de Campeão.

Objetivo: Aumentar a barreira de acesso de contas ilícitas ao modo Por Colocação e ao rank de Campeão, a fim de promover um melhor ambiente para o modo Por Colocação e um placar limpo ao rank de Campeão.

Status: (Meta: Y5S2) Teremos mais detalhes quanto aos requisitos com a chegada da Segunda Temporada

LIMITE NO NÍVEL DE XP DIÁRIO EM PVE
Descrição: Limite no ganho de nível diário em PvE como medida de prevenção para tornar os requisitos do modo Por Colocação menos acessíveis a contas ilegítimas.

Objetivo: Nossa meta é tornar o modo Por Colocação menos acessível a bots e smurfs limitando a XP que pode ser ganha em PvE. Isso nos ajudará a localizar e deter bots e smurfs antes que possam avançar no modo Por Colocação.

Status: A intenção é implementar essa medida assim que possível.

FALHAS/PROBLEMAS CONHECIDOS
FALHA DE SANÇÕES POR ABANDONO INDEVIDAS
Descrição: Encontramos um erro na Organização de Partidas que fazia com que os jogadores recebessem sanções de abandono se deixassem a fila do modo Por Colocação assim que encontrada uma partida. Considerava-se que os usuários estavam na partida, porém, estes já tinham retrocedido.

Status: Meta: Y5S2) Uma correção está programada para a Y5S2.

FALHA DE EQUIPES INCOMPLETAS NO INÍCIO DE PARTIDAS POR COLOCAÇÃO
Descrição: Havia um erro na Organização de Partidas que fazia com que elas começassem com equipes incompletas se um jogador deixasse a fila logo após uma partida ser encontrada. Esses jogadores eram processados como se estivessem na partida, quando na verdade tinham retrocedido, resultando em jogos com equipes incompletas.

Status: (Meta: Y5S2) Uma correção está programada para a Y5S2.

FALHA "EM BREVE" PARA DESAFIOS DA COMUNIDADE JÁ CONCLUÍDOS NO PASSE DA BATALHA
Descrição: Desafios antigos da comunidade apareciam como "Em breve" no menu do Passe da Batalha após o final do desafio. Esta é uma falha meramente visual.

Status: (Meta: Y5S2) Uma correção para a falha está programada para a atualização do Passe da Batalha da Y5S2.

FALHA DE JANELA DE REDUÇÃO DE MMR
Descrição: As janelas que notificam os jogadores de uma redução de MMR depois que um cheater fosse banido exibiam valores incorretos. Esta é uma falha meramente visual.

Status: (Meta: Y5S3) Atualmente sob investigação.



DESEMPENHO DA ANÁLISE COMPARATIVA
Descrição: Este recurso aprimora a atual ferramenta de análise comparativa de hardware para PC em Siege, trazendo informações detalhadas e relatórios de performance sobre as especificações de hardware e o uso de recursos.

Objetivo: Trabalhamos em direção a um novo teste comparativo que melhor expressa como Siege desempenha em configurações diferentes.

Status: (Meta: Y5S2).

CONSISTÊNCIA EM GADGETS DESTRUINDO OBJETOS
Descrição: Há inconsistências na maneira como gadgets interagem/destroem objetos nos mapas.

Status: (Meta: Y5S3) De maneira similar a quando trouxemos consistência a como os objetos se rompem na trajetória de projéteis em Y4S4, estamos trabalhando num sistema para a interação de gadgets aplicáveis com esses objetos.

EQUIPES INCOMPLETAS NO INÍCIO DE PARTIDAS POR COLOCAÇÃO
Descrição: Partidas Por Colocação às vezes começavam com equipes incompletas devido a jogadores que saíam no início das disputas.

Objetivo: Para tratar das frustrações de quando jogadores são colocados numa partida com equipes incompletas em algum lado, estamos trabalhando num recurso de Cancelamento de Partida. O objetivo é permitir que jogadores cancelem partidas num estágio inicial, desde que seu time esteja incompleto.

Status: (Meta: Y5S2 no servidor de testes para PC) O Cancelamento de Partida está programado para ser lançado no servidor de testes para PC na Y5S2

NOTIFICAÇÃO E FEEDBACK PARA AGENTES DESATIVADOS FORAM APRIMORADOS
Descrição:__ Notificações e feedback in-game aprimorados para quando um jogador tenha sido desativado.

Status: (Meta: Y5S3) Houve casos em que tivemos de desativar um Agente para melhorar a experiência de jogo quando surgiram exploits. Estamos trabalhando para oferecer melhor feedback a nossos jogadores quando uma ocasião dessas surgir.