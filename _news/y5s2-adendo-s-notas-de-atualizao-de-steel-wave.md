---
title: 'Y5S2: ADENDO ÀS NOTAS DE ATUALIZAÇÃO DE STEEL WAVE'
excerpt: 'Durante o período de testes de Steel Wave, recebemos uma série de pedidos de ajustes de equilíbrio para os novos Agentes. Depois de avaliarmos o feedback dos jogadores e os dados dos servidores de testes, planejamos efetuar algumas modificações de equilíbrio nos novos Agentes com o patch 2.1. As mudanças não terão efeito até a atualização Y5S2.1.'
img: '/news2.JPG'
date: '2020-06-15'
---

## EQUILÍBRIO
## ATUALIZAÇÃO Y5S2.1

Durante o período de testes de Steel Wave, recebemos uma série de pedidos de ajustes de equilíbrio para os novos Agentes. Depois de avaliarmos o feedback dos jogadores e os dados dos servidores de testes, planejamos efetuar algumas modificações de equilíbrio nos novos Agentes com o patch 2.1. As mudanças não terão efeito até a atualização Y5S2.1.

Melusi - Chegada em Y5S2.1

Zona de penalização reduzida em cerca de 33%.
Redução no volume do efeito da Banshee em 1ª pessoa.
Ace e Hibana - Chegada em Y5S2.1

Eliminamos a possibilidade de destruir as baterias de Bandit através de reforços colocando as cargas no piso.
MELUSI
Substituímos seu escudo extensível por C4.
Depois de reavaliarmos alguns metas do jogo e fatores de jogabilidade atuais, decidimos trocar o escudo extensível de Melusi por C4, a fim de evitar pressão extra nos atacantes e utilidade de ataque específica.

ACS12
Aumentamos o dano de 41 para 59. Aumentamos a velocidade ADS.
A ACS12 agora tem 300 RPM.
Buff no dano causado pela ACS12 para torná-la um pouco mais viável. A velocidade ADS (disparo com mira) ficará de acordo com outras armas da mesma categoria. Além disso, no lançamento, a ACS12 tinha 300 RPM (disparos/minuto), mas sofreu redução numa mudança de equilíbrio posterior. Com a nova atualização, juntamente com a mudança para balas, destruição não será problema para os 300 RPM.

ATUALIZAÇÕES
ATUALIZAÇÕES NAS LOCALIZAÇÕES DE ÁUDIO
A localização do áudio em francês para Ace e Melusi estará disponível no lançamento. A localização em russo ainda segue indisponível.

ADICIONAIS PARA ARMAS
Atualizações no sistema de adicionais conferem maior equilíbrio nas estampas para adicionais e loadout simplificado.

DETECTORES DE METAIS
Ajuste nos efeitos sonoros dos detectores de metais em Banco, Canal e Fronteira.

Agora emitem um alarme de 3s antes de parar.
Somente um alarme toca por vez.
Sem tempo de espera na ativação do alarme.
As medidas corrigem uma falha pela qual o tempo de espera permitia que jogadores cruzassem detectores de metais várias vezes sem que o alarme soasse (se já tivesse soado antes e estivesse em espera.

RESIDÊNCIA
Adicionamos um bloqueio na janela superior e barras na parede em frente à casa da árvore para permitir que atirem por ali; porém, os jogadores não podem cruzar a janela pelo rapel.

CORREÇÕES DE FALHAS
GAMEPLAY
CONSERTADO – Os disparos da ACS12 não causavam dano a distâncias acima de 40m.
CONSERTADO – A animação e o áudio de recarregamento da M4 estavam dessincronizados.
CONSERTADO – A animação e o áudio de recarregamento da AK12 estavam dessincronizados.
CONSERTADO – O FPS caía quando destroços voavam perto dos jogadores.
CONSERTADO – A carcaça da câmera à prova de balas ainda era fixada depois do cancelamento ao se agachar.
CONSERTADO – Escudos extensíveis (e o Vulcan do Goyo) tinham prioridade sobre outros gadgets arremessáveis. Os gadgets devem ser pegos de acordo com o mais recentemente usado.
CONSERTADO – Em algumas situações, os jogadores podiam saltar sobre outro Agente.
CONSERTADO – A área de acerto do contêiner biológico era grande demais em Proteger Área.
CONSERTADO – Um agente com escudo “abraçando” a parede poderia impedir a fixação de gadgets de detonação pesada na camada inferior da parede reforçada (como a carga de demolição do Thermite e o Espelho Negro da Mira).
CONSERTADO – As granadas de impacto e as granadas da Zofia explodiam no atacante e não em seu escudo balístico caso ele estivesse correndo/andando no momento do impacto.
CONSERTADO – A descoberta do objetivo não era acionada ao olhar para ele indiretamente do canto da tela com as configurações de campo de visão acima de 60.
CONSERTADO – O zoom no modo ADS parecia reduzido para algumas miras com a P10 Roni.
CONSERTADO – O refém ficaria desacordado, ao invés de morrer como esperado, se um gadget explosivo detonasse perto dele.
AGENTES
CONSERTADO – Efeitos sonoros anômalos nos disparos da T-95 LSW.
CONSERTADO – Ausência de efeitos visuais ao usar armas e adicionais específicos (por exemplo: M12 + Freio de Boca: o resultado eram apenas faíscas, ao invés de um clarão e fumaça).
CONSERTADO – Diversas pequenas falhas de animação ao realizar certos movimentos com vários Agentes.
CONSERTADO – Diversas pequenas correções visuais e atualizações para o modo de suporte e replays de eliminação.
CONSERTADO – Diversas pequenas correções para Agentes e seus gadgets.
ACE

CONSERTADO – O SELMA não destruía arame farpado na primeira explosão.
CONSERTADO – O SELMA podia se fixar no cânister vermelho do Espelho Negro, interferindo na orientação da animação.
CONSERTADO – Ace não recebia pontos depois de destruir um gadget fixável dos Defensores com o SELMA.
MELUSI

CONSERTADO – As Banshees de Melusi deveriam ter a mesma resistência à explosão que os Evil Eyes de Maestro. Elas tinham resistência bastante baixa às detonações.
AMARU

CONSERTADO – Algumas vezes, Amaru não podia subir até uma abertura, mesmo se o ícone de ativação do gadget estivesse disponível.
CONSERTADO – O Gancho Garra não se prendia corretamente aos pontos esperados nas claraboias.
CONSERTADO – Amaru podia usar seu lançador Garra na janela bloqueada em EXT Casa da árvore, no mapa Residência.
CONSERTADO – Pequeno efeito visual de fumaça nos cantos onde o Gancho Garra se fixava na moldura da janela.
CONSERTADO – Nos replays, o efeito sonoro do rebobinar apresentava distorção ou não era reproduzido.
BANDIT

CONSERTADO – Destroços do piso e aberturas reforçadas parcialmente danificadas podiam impedir Bandit de fixar seu gadget.
CAPITÃO

CONSERTADO – O fogo não propagava devidamente quando os dardos eram disparados sob alguns móveis.
ECHO

CONSERTADO – Drones de choque e o Yokai podiam disparar contra Agentes através das barricadas normais e das de Castle.
CONSERTADO – A visão da câmera do Yokai ficaria escurecida e sem cor se Thatcher usasse seu EMP em qualquer lugar do mapa.
CONSERTADO – Em alguns mapas, o Yokai podia atravessar tetos finos, tendo vista para os telhados, no modo suporte.
FINKA

CONSERTADO – O gadget no dedão de Finka não apresentava os efeitos visuais do marcador ao usar o último pico de adrenalina.
FUZE

CONSERTADO – Um Agente poderia receber dano ou até morrer se estivesse próximo à explosão de um perfurador, mesmo fora da área de efeito do gadget.
GLAZ

CONSERTADO – A mira térmica não tinha visibilidade através da fumaça, mesmo quando totalmente carregada. A marca amarela ainda era visível, porém mais fraca do que deveria.
CONSERTADO – A OTs-03 podia ser usada com total funcionalidade ao escoltar o refém.
IANA

CONSERTADO – Seu holograma ativaria o sistema de estilhaços.
LESION

CONSERTADO – Lesion podia ver pela fumaça ao olhar através de uma Mina GU instalada.
LION

CONSERTADO – O EE–ONE–D podia ser atingido e destruído por disparos.
NOKK

CONSERTADO – Se Nokk tivesse seu Redutor de Presença HEL ativado e recebesse dano do SELMA do ACE, o efeito visual não seria exibido a quem operasse um Evil Eye ou uma câmera à prova de balas.
MAESTRO

CONSERTADO – Se Maestro deixasse a partida, as janelas de seus Evil Eyes não abririam 50% quando desativados.
WARDEN

CONSERTADO – Warden não exibia sua animação típica de recarregamento com a SMG-12.
YING

CONSERTADO – As Candelas não ativavam corretamente depois de jogadas através de uma parede eletrificada e danificada.
ZOFIA

CONSERTADO – As granadas de impacto e concussão do gadget da Zofia não afetavam inimigos à curta distância.
DESIGN DE NÍVEIS
CONSERTADO – Passagens, paredes reforçadas e alçapões de diversos mapas ficavam bloqueados se um jogador estivesse ausente no início.
CONSERTADO – Diversas oportunidades para spawn kill/spawn peek em vários mapas.
CONSERTADO – Problemas com a resistência do vidro de alguns objetos aos gadgets.
CONSERTADO – Várias falhas de colisão nos mapas.
CONSERTADO – Várias falhas de clipping/clipping dinâmico nos mapas.
CONSERTADO – Várias falhas no nível de detalhes (LOD) nos mapas.
CONSERTADO – Várias falhas na linha de visão (LOS) nos mapas.
CONSERTADO – Várias falhas de destruição de objetos dos mapas.
CONSERTADO – Alertas para saltar desnecessários/ausentes.
CONSERTADO – Várias falhas com objetos dos mapas.
CONSERTADO – Várias falhas de iluminação/textura nos mapas.
RESIDÊNCIA

CONSERTADO – Se os Defensores tivessem ponto de início em 1º Sala de TV e 1º Sala de música, um desses jogadores iniciaria em P Academia.
CONSERTADO – Os jogadores não podiam pegas as Pestes de Mozzie se fossem fixadas sobre o tapete nas escadas.
CONSERTADO – Gadgets podiam ser instalados em áreas inacessíveis da versão reformulada de Residência.
CONSERTADO – Os Agentes poderiam entrar na estante de livros em 2º Sala de leitura.
CONSERTADO – Uma parede em 2º Quarto rosa não poderia ser reforçada no Modo Refém se um jogador que iniciasse perto dela não se movesse.
CONSERTADO – Jogadores que subissem de rapel pela janela de 2º Escada traseira permaneceriam no ar por um instante por um problema no piso.
CONSERTADO – Os Defensores poderiam bloquear uma parede reforçada em 2º Quarto do carrinho se estivessem ausentes ao iniciar.
CONSERTADO – Os drones não iniciavam próximo ao local escolhido.
CONSERTADO – Pequenos gadgets utilizáveis não podiam ser pegos novamente quando fixados na almofada da poltrona em 2º Suíte master.
CONSERTADO – Drones podiam ficar presos na pilha de canos em frente à construção.
FRONTEIRA

CONSERTADO – Um dos alçapões em 2º Sala do servidor não era totalmente destruído e poderia causar falha de colisão nos jogadores.
CHALÉ

CONSERTADO – “Pixel peek” a partir de P Adega de vinhos.
CONSULADO

CONSERTADO – O Prisma da Alibi não seria instalado quando lançado no jardim interno em 1º Escadaria principal.
CONSERTADO – Cargas de demolição poderiam ser fixadas em áreas indestrutíveis do piso em 2º Corredor.
CONSERTADO – Ausência de colisão para as cargas do X-Kairos de Hibana e a escrivaninha am 1º Saguão.
CONSERTADO – Ponto de rapel incorretamente localizado na claraboia acima de 1º Escada principal.
CONSERTADO – Os gadgets dos Agentes poderiam ficar presos nas bandeiras.
CLUBE

CONSERTADO – A instalação do desativador em certos pontos da prateleira metálica em P Arsenal poderia causar uma falha que faria com que os Atacantes perdessem a rodada.
CONSERTADO – Em Garagem das motoneves, os jogadores podiam destruir as caixas de papelão e se esconder nas sombras.
CONSERTADO – Girar em certos pontos do mapa poderia causar a perda de FPS a outros jogadores.
LITORAL

CONSERTADO – Os Atacantes podiam esconder o desativador dentro do armário em 2º Cobertura.
FAVELA

CONSERTADO – Os Trax Stingers de Gridlock eram colocados dentro das máquinas de lavar em 1º Lavanderia.
FORTALEZA

CONSERTADO – Gadgets e granadas podiam ficar presos sob a mesa de totó em 2º Salão de jogos quando lançados de certos ângulos.
CONSERTADO – Quando jogadores atiravam um dardo do Capitão em certos pontos do exaustor, o fogo acendia, mas não se propagava.
CONSERTADO – Problema ao reforçar uma parede em 2º Salão de jogos quando algum dos Defensores estava ausente.
CONSERTADO – Os jogadores ainda poderiam receber choque através de uma parede indestrutível em 2º Corredor de shisha.
HEREFORD

CONSERTADO – Os jogadores podiam se esconder atrás de um boneco numa estande em 1º Garagem.
OREGON

CONSERTADO – Vidros ausentes em algumas janelas.
CANAL

CONSERTADO – Jogadores podiam ficar presos dentro da ventilação em EXT Beco da empilhadeira após saírem do rapel entre o vão nas saídas de ar.
CONSERTADO – O drone Yokai poderia perder o sinal e parar de responder se usado em cima da ventilação em 2º Sala da impressora.
EXPERIÊNCIA DO USUÁRIO
CONSERTADO – Várias melhorias na interface do usuário nos menus.
CONSERTADO – Várias pequenas correções cosméticas e na interface do usuário na Loja.
CONSERTADO – Várias pequenas correções e melhorias no menu.
CONSERTADO – Várias correções cosméticas em amuletos, uniformes e headgears.
CONSERTADO – Cartões de jogador vazios visíveis no modo espectador se todas as ferramentas de observação fossem destruídas e o jogador entrasse numa partida já iniciada.
CONSERTADO – Drones flutuantes nos modos suporte e espectador.
CONSERTADO – O contorno de objetivos e alguns gadgets ficava em destaque quando no modo suporte, depois de morrer.
CONSERTADO – Várias melhorias e atualizações no display de navegação do espectador. Aumentamos a legibilidade dos estados do jogador nos cartões de jogador.
CONSERTADO – Falhas de replicação visual em 3ª pessoa para Agentes saindo do rapel de um telhado.
CONSERTADO – Os efeitos de flash não apareciam para jogadores no modo suporte.
CONSERTADO – Armas atravessavam as mãos dos Defensores no início se algum suporte estivesse equipado.
CONSERTADO – (PvE) Ausência das barricadas no exterior no início do Cenário 02.
CONSERTADO – (PvE) Um bot poderia ficar preso em frente ao bar em 1º Restaurante depois que o desativador fosse plantado.
CONSERTADO – Em PvE, os adicionais eram invisíveis nos bots.
CONSERTADO – Os jogadores não podiam completar o objetivo “Matar um inimigo cegado no segundo Cenário.
CONSERTADO – A carga da CPU constava como 0% nos resultados da análise avançada.
CONSERTADO – As opções 'Nenhum'/'Padrão' para adicionais, amuletos, headgear e uniformes estavam ausentes.
CONSERTADO – Na playlist Descoberta, faltava um nome em alguns dos elementos da interface do usuário no menu principal.
CONSERTADO – Carregamento infinito no modo de jogo online personalizado.
CONSERTADO – O abandono de uma partida durante as fases de preparação ou banimento causavam a descida na colocação para Cobre V para quem já tivesse colocação.
CONSERTADO – A textura para a estampa de adicional de armas preta era mais escura nas miras holográficas/à laser.