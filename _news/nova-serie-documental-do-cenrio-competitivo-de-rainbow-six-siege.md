---
title: 'NOVA SÉRIE DOCUMENTAL DO CENÁRIO COMPETITIVO DE RAINBOW SIX SIEGE!'
excerpt: 'A Ubisoft Brasil lançou nesta quarta-feira (10) a série documental “Ascensão: O Caminho para um Sonho”. Dividida em oito episódios, a produção mostrará em cada um deles a trajetória de um atleta do país até tornar-se um profissional da modalidade. Todos os personagens envolvidos estão em equipes que disputarão o Brasileirão Rainbow Six Siege 2020.'
img: '/news1.jpg'
date: '2020-06-22'
---

## Lorem Ipsum

A Ubisoft Brasil lançou nesta quarta-feira (10) a série documental “Ascensão: O Caminho para um Sonho”. Dividida em oito episódios, a produção mostrará em cada um deles a trajetória de um atleta do país até tornar-se um profissional da modalidade. Todos os personagens envolvidos estão em equipes que disputarão o Brasileirão Rainbow Six Siege 2020.

<button type="button" class="video-wrapper-thumbnail"><img loading="lazy" src="https://i.ytimg.com/vi/CRciAgDoS0U/maxresdefault.jpg" alt="youtube video: CRciAgDoS0U"></button>

Os episódios serão lançados às segundas, quartas e sextas-feiras no canal oficial do Rainbow Six Esports Brasil no YouTube e também nas redes sociais. O primeiro deles contará a história de André “NESKWGA”, da Team Liquid, e vai ao ar nesta quarta-feira (10), às 12h.

Além de Nesk, Gabriel “cameram4n” Hespanhol (FaZe Clan), Murilo “muzi” Moscatelli (Ninjas in Pyjamas), José "Bullet1" Victor (MIBR), João "DRUNKKZZ" Giordano (INTZ), Luca "LuKid" Sereno (Team oNe), Pedro "pzd" Dutra (Black Dragons) e Luccas "Paluh" Molina (Team Liquid) estarão presentes na série que levará ao público a trajetória e os desafios que eles precisaram superar em busca de seus sonhos.

A produção é composta por depoimentos de familiares, amigos, colegas de equipe e dos próprios atletas, além de imagens exclusivas de suas carreiras. Ela mostrará os profissionais de Rainbow Six Siege por outra perspectiva ao abordar os diferentes caminhos que eles tomaram em suas vidas e que os levaram ao cenário competitivo.

O documentário foi produzido pela Ubisoft Brasil em parceria com o jornalista Leo Bianchi, que trabalhou como o diretor e roteirista no projeto. “Foi um trabalho ousado diante da realidade que temos no esporte eletrônico brasileiro. Esse tipo de conteúdo focado nas histórias de pro players só víamos, até então, em produções estrangeiras. Todo indivíduo que escolheu essa nova profissão enfrentou um drama e teve de abrir mão de algo para apostar nesse sonho. Poder documentar essas histórias foi muito enriquecedor do ponto de vista jornalístico e, além disso, vai quebrar muitos paradigmas sobre os jogadores, especialmente para quem é pai dessa molecada gamer da atual geração”, destaca Leo.

A estreia levará aos fãs a história de Nesk que, com origem humilde, precisou superar adversidades ao lado de seus familiares para se tornar um dos jogadores mais respeitados e conhecidos do mundo, tendo no currículo um título mundial da Pro League em 2018 - quando foi escolhido como MVP do torneio - além do troféu de campeão brasileiro, alcançado em 2019, entre outros. Assista ao primeiro episódio completo clicando aqui.

“Será uma oportunidade para os fãs de Rainbow Six Siege se aproximarem ainda mais de seus ídolos e conhecê-los para além do jogo. Iniciativas como essa só fortalecem ainda mais nosso cenário profissional e ajudam na construção do legado do R6, além de servir de exemplo para jovens que sonham em seguir carreira nos esportes eletrônicos”, diz Marcio Canosa, Diretor de Esports da Ubisoft para América Latina.

Anteriormente, a Ubisoft já havia produzido outros títulos que retratam o competitivo do jogo. O filme “Em Busca da Vitória”, lançado em 2019, contou a jornada de alguns dos maiores jogadores do mundo, entre eles o brasileiro Leo “ziG” Duarte, até o Six Invitational daquele ano. Em 2018, a produtora do game exibiu o documentário “Por Outro Ângulo”, que contou como o Rainbow Six Siege influenciou a vida de diversos personagens do cenário.