---
title: 'Brasileirão Rainbow Six Siege 2020 começa no dia 2 de julho com premiação de meio milhão'
excerpt: 'O Brasileirão Rainbow Six Siege 2020 já tem data de estreia definida. O mais importante campeonato nacional da modalidade terá início no dia 2 de julho (quinta-feira), e chega repleto de novidades, com um novo corpo de talentos, mais partidas ao longo da semana e premiação total de R$500 mil.'
img: '/news10.png'
date: '2020-06-25'
---

# Brasileirão Rainbow Six Siege 2020 começa no dia 2 de julho com premiação de meio milhão de reais

_Quarta edição do torneio contará com premiação recorde, mais jogos durante a semana e corpo de talentos ampliado_

O Brasileirão Rainbow Six Siege 2020 já tem data de estreia definida. O mais importante campeonato nacional da modalidade terá início no dia 2 de julho (quinta-feira), e chega repleto de novidades, com um novo corpo de talentos, mais partidas ao longo da semana e premiação total de R$500 mil.

O torneio será disputado na Max Arena, localizada no bairro da Mooca, em São Paulo. Devido à pandemia causada pela COVID-19, a Ubisoft Brasil definiu que o primeiro turno do BR6 será feito de maneira remota, sem a presença de equipes e torcedores, com apenas o staff técnico da organização no local para a realização das transmissões. As medidas de segurança foram tomadas com o intuito de garantir a saúde e o bem-estar de todos os envolvidos no projeto.

A quarta edição da competição contará com 10 equipes na briga pelo título. São elas: Team Liquid, Faze Clan, Ninjas in Pyjamas, MIBR, Black Dragons, INTZ e-Sports, Team oNe, FURIA Esports, Santos e-Sports e W7M Gaming. Os times entrarão na disputa pela maior premiação da história do Brasileirão até aqui.


> “O Brasileirão de 2020 é o maior até hoje em número de participantes e valor de premiação. Nossa expectativa é de que seja uma edição inesquecível e que marque um novo ciclo ainda mais promissor e glorioso para a modalidade”

*Destaca Marcio Canosa, Diretor de Esports da Ubisoft para América Latina.*

Confira os valores que cada organização pode faturar de acordo com sua posição final:
**
* 1º - R$200 mil
* 2º - R$80 mil
* 3º - R$60 mil
* 4º - R$60 mil
* 5º - R$40 mil
* 6º - R$20 mil
* 7º - R$20 mil
* 8º - R$10mil
* 9º - R$10mil
* 10º - Não recebe premiação

Vale lembrar que o campeonato terá dois turnos em 2020. O primeiro acontecerá entre julho e agosto, e o segundo em setembro e outubro. Os jogos serão às quintas, sábados e domingos, a partir das 13h, com transmissão ao vivo dos canais do Rainbow Six Esports Brasil na [Twitch](twitch.tv/R6esportsBR) e no [YouTube](youtube.com/R6esportsBR).

Serão três partidas por dia no formato MD2 (melhor de dois mapas). Desta forma, vitórias por dois mapas a zero valem três pontos na tabela de classificação e vitórias por um mapa a zero, dois pontos. Empates por um a um ou zero a zero valem um ponto.

<br>

![](https://i.imgur.com/8xJERf0.jpg)_Confira as datas de todas as partidas do primeiro turno do campeonato:_

## Novos talentos nas transmissões

Outro ponto que chamará a atenção dos fãs será o novo corpo de talentos dos canais oficiais do Rainbow Six Esports Brasil, que comandará as transmissões do BR6 e das outras ligas estrangeiras espalhadas pelo circuito competitivo mundial. Desta forma, o público terá mais de 70 horas semanais de conteúdo.

Assim como o Brasileirão 2020, o canal também vai transmitir todas as emoções da Liga Europeia e da Liga Americana às segundas e quartas-feiras. Nas quintas e sextas-feiras, será a vez da Liga Sul-Americana. Para encerrar a semana, os jogos da Liga Mexicana acontecem aos sábados e domingos, logo após o BR6.

Além de André “Meligeni”, Otávio “Retalha”, Ricardo “qeP” e Victória “Viic”, figuras já conhecidas pela comunidade, as partidas de Rainbow Six Siege contarão com a presença de novos narradores, comentaristas, analistas e apresentadores.

Com experiência nas transmissões de diversas modalidades como Overwatch, League of Legends e Call of Duty, o caster Felipe “Tonello” chega para integrar o time de narradores e comandará as emoções das ligas Sul-Americana e Mexicana. Ele terá ao seu lado o ex-pro player e novo comentarista da Ubisoft, Vitor “IntacT” Janz, que acumula passagens de sucesso por grandes organizações do cenário competitivo brasileiro.

Os novos apresentadores do BR6 possuem currículos de peso. O jornalista Leo Bianchi, que tem passagem pela Rede Globo e atualmente é colunista do UOL, é o primeiro deles. O profissional também foi diretor e roteirista da série documental “Ascensão: o caminho para um sonho”, lançada pela Ubisoft Brasil neste mês.

Ao lado dele estará a jornalista Domitila Becker, que tem passagens pela Revista Veja e pelos canais SporTV, onde foi repórter e apresentadora. Quem completará o time será a repórter e apresentadora Mirelle Moschella, do Grupo Bandeirantes de Comunicação. A profissional já possui vasta experiência na coberturas de grandes eventos esportivos como, por exemplo, duas Copas do Mundo e dois Jogos Olímpicos.

Além deles, o Brasileirão também contará com a presença de um novo analista: Luiz “IqueGMK” Monclar. O streamer de R6 faz parte da comunidade de fãs do jogo e trará sua visão analítica para as partidas do torneio nacional.

_Veja abaixo os nomes e funções que cada membro da equipe de transmissão exercerá:_
![](https://i.imgur.com/WcO0epa.png)

#### Narradores:

André “Meligeni” - Brasileirão e Liga Europeia
Ricardo “qeP” - Brasileirão e Liga Norte-Americana
Felipe “Tonello” - Campeonatos Sul-Americano e Mexicano

#### Comentaristas:

Otávio “Retalha” - Brasileirão e Liga Europeia
Victória “Viic” - Brasileirão e Liga Norte-Americana
Vitor “IntacT” - Campeonatos Sul-Americano e Mexicano

#### Analista:

Luiz “Ique” Monclar - Brasileirão

#### Apresentadores:

Leo Bianchi - Brasileirão
Domitila Becker - Brasileirão
Mirelle Moschella – Brasileirão
<br>
### Sobre Tom Clancy's Rainbow Six Siege

Inspirado em organizações antiterroristas do mundo real, o Tom Clancy's Rainbow Six Siege coloca seus jogadores no meio de confrontos letais frente a frente. Pela primeira vez em um jogo Tom Clancy's Rainbow Six, jogadores vão invadir cercos, um novo estilo de invasão em que inimigos têm os meios para transformar seus ambientes em fortalezas modernas enquanto os times Rainbow Six lideram uma invasão para conquistar a posição inimiga. Tom Clancy's Rainbow Six Siege dá aos jogadores controle sem precedentes sobre a habilidade de fortificar a sua posição reforçando paredes e pisos, usando arame farpado e reforços implantáveis, colocando minas, ou invadir a posição inimiga usando drones de observação, cargas explosivas, rapel, entre outros. O ritmo acelerado e a singularidade de cada cerco estabelecem um novo padrão para tiroteios intensos, jogabilidade estratégica e jogos competitivos.

### Sobre a Ubisoft

A Ubisoft é uma empresa líder na criação, publicação e distribuição de entretenimento e serviços interativos, com um rico portfólio de marcas de renome mundial, incluindo Assassin's Creed, Far Cry, For Honor, Just Dance, Watch Dogs e a série de jogos Tom Clancy incluindo Ghost Recon, Rainbow Six e The Division. As equipes da rede mundial de estúdios e escritórios de negócios da Ubisoft estão comprometidas em oferecer experiências de jogos originais e memoráveis em todas as plataformas populares, incluindo consoles, telefones celulares, tablets e PCs. Para o ano fiscal de 2018-19, a Ubisoft gerou receitas líquidas de € 2.029 milhões. Para saber mais, visite [www.ubisoftgroup.com](http://www.ubisoftgroup.com).
