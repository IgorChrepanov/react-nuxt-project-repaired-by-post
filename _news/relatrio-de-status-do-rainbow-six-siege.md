---
title: 'RELATÓRIO DE STATUS DO RAINBOW SIX SIEGE'
excerpt: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Praesent elementum facilisis leo vel fringilla est ullamcorper eget. At imperdiet dui accumsan sit amet nulla facilisi morbi tempus.'
img: '/news4.JPG'
date: '2020-06-11'
---

## Índice:
1. Problemas sonoros
2. Conectividade
--- 2.1. Conectividade em geral
--- 2.2. Política de latência
3. Cheats e comportamento disruptivo
--- 3.1. DDoS
--- 3.2. Potencializadores
--- 3.3. Smurfing
--- 3.4. Equipes incompletas
--- 3.5. Abuso na votação para expulsão
--- 3.6. Frustação com a redução de MMR
--- 3.7. Modo Streamer
4. Acessibilidade
5. Falhas na propagação de fumaça
6. Problemas com drones
Siege é um jogo que já tem uma longa história e estamos cientes de que existem problemas antigos que ainda precisamos resolver de forma definitiva. Estamos nos dedicando para manter e evoluir o jogo continuamente, como já fizemos por muitos anos. Agradecemos o apoio da comunidade por todo esse tempo e também somos gratos pela sua paciência enquanto o jogo segue em desenvolvimento.

Neste Relatório de Status, vamos debater alguns assuntos de longa data que preocupam os jogadores e são de extrema importância para nós, mas que exigem uma discussão complexa. As nossas postagens regulares no blog sobre Principais Falhas e Apreensões da Comunidade abordam correções mais definitivas e de curto prazo. O objetivo aqui é compartilhar informações dos bastidores sobre o motivo de alguns problemas antigos serem tão persistentes e quais soluções estamos buscando em longo prazo.

Todos esses tópicos serão abordados em um futuro próximo, porém já temos várias providências em andamento que gostaríamos de compartilhar agora.

1. PROBLEMAS SONOROS
Apreensões: Estamos cientes de várias falhas sonoras que vêm incomodando os jogadores há algum tempo. O sistema de som do Siege é bastante complexo e visa uma simulação mais realística de propagação de som em ambientes 3D. Em alguns casos, a complexidade do sistema sonoro pode ser confusa e causar alguns mal-entendidos — por exemplo, às vezes um som usa uma rota específica para chegar ao jogador, mas este jogador não sabe que tal rota existe (como no caso das aberturas do Maverick). Em outros casos, essa complexidade também pode dificultar a percepção de bugs sonoros, tais como os sons faltantes ou áudios invertidos que são mencionados frequentemente.

Desafios: O nosso sistema de som leva vários parâmetros em consideração ao acionar sons para os jogadores. Isso inclui: posição do jogador, ambiente, situação no jogo, entre outros. A cada temporada, adicionamos componentes novos que interagem com o sistema sonoro, e com esses elementos novos sempre há chances de introduzir bugs novos.

Outro fator importante a ser considerado é que o motor de destruição no Siege possibilita um ambiente dinâmico de alteração constante. Cada rodada é única, pois a propagação de som é alterada dinamicamente com base nos diversos estados de destruição. Isso adiciona uma camada extra de complexidade para reproduzir e corrigir falhas sonoras.

O objetivo da propagação sonora no Siege é relacionar o direcionamento do som com a posição dos jogadores de uma forma realística. Isso é feito ao dividir os mapas em vários espaços, o que permite o som a se propagar de um espaço para o outro através de pontos conectores (tais como paredes destrutíveis, pisos, portas e janelas). Então, dependendo do estado de destruição atual do mapa, o sistema de propagação conecta o som ao ouvinte calculando a melhor rota. O sistema verifica se os pontos de conexão estão tapados pelas respectivas paredes ou se alguma abertura já foi feita, o que permite a passagem do som. Assim que o algoritmo determina o melhor caminho, usamos essas informações para posicionar e modular o som para que os jogadores possam reconhecer a origem e a distância do som.

Futuro: A fim de melhorar a situação em Siege, tomamos várias medidas internas para auxiliar no diagnóstico e na correção de problemas sonoros. Em primeiro lugar, instrumentamos o sistema de som para que ele nos auxilie a detectar os problemas mais rápido. Com isso buscamos identificar os erros mais cedo no processo de desenvolvimento, além de depurar os problemas de difícil reprodução.

Em segundo lugar, revisamos a situação dos nossos sistemas de som e de propagação. Chegamos à conclusão de que precisamos refazer grande parte deles, começando pela forma em que os sons são agrupados e conectados ao jogo. Visamos relançar as primeiras partes dos sons reagrupados na Temporada 3. Entretanto, a reconstrução completa dos sistemas de interação e propagação de som é uma empreitada de grandes proporções e não ficará pronta no Ano 5.

Nesse meio-tempo, bugs novos que aparecerem serão corrigidos quando a reprodução adequada for alcançada pelas nossas equipes de controle de qualidade. Essa é uma das maiores dificuldades por trás dos bugs de áudios inversos e ausentes: eles parecem ocorrer aleatoriamente e não podem ser reproduzidos com consistência de forma proposital, o que dificulta bastante o trabalho em busca da solução. No entanto, vamos continuar tentando reproduzir os bugs sonoros como parte dos nossos processos rotineiros e corrigi-los assim que possível. Também gostaríamos de pedir ajuda à comunidade quanto a essa questão. Se vocês encontrarem qualquer forma consistente de reproduzir qualquer problema sonoro que esteja acontecendo, por favor, informem a gente através do R6 Fix — selecione a opção de contato direto no seu envio e vamos retornar para mais informações.

2. CONECTIVIDADE
Conectividade é um tópico variado que abrange diversas áreas, tais como provedores de serviço de Internet, nossos serviços de backend (ex.: organização de partidas), servidores do jogo, provedor dos servidores e até mesmo ataques DDoS.

2.1 CONECTIVIDADE EM GERAL
Desafios: As nossas equipes de atividades online e ao vivo monitoram os serviços online 24 horas por dia para garantir que o jogo sempre esteja acessível aos jogadores. Eles sempre estão cientes de qualquer degradação que possa acontecer e trabalham duro para resolver os problemas o mais rápido possível. Porém, degradações acontecem apesar dos esforços deles. Embora algumas degradações possam ocorrer devido a alterações do nosso lado, outras estão totalmente fora do nosso controle (ex.: degradações de terceiros). Quando uma degradação acontece, fazemos o nosso melhor para restabelecer os serviços online com impacto mínimo para os jogadores. Para casos de períodos longos offline, temos uma matriz de compensação.

Por exemplo, no começo das medidas de isolamento em meados de março, a carga extra nos nossos servidores causou degradações na organização de partidas e problemas de login. Em resposta ao aumento de jogadores, escalamos a nossa infraestrutura tomando as seguintes medidas:

Dobrando o número de servidores alocados para processar os dados de organização de partidas (distribuir a carga para mais servidores a fim de evitar backlogs).
Adicionando shards extras (cada shard é uma instância de servidor separada) para aumentar a capacidade dos bancos de dados (aumentando escalabilidade e desempenho).
Dobrando a capacidade do balanceador de carga do Rainbow Six Siege (aumentando a capacidade de distribuição de tráfego na rede de forma mais confiável).
Os problemas de conectividade duraram menos de uma semana do começo ao fim, enquanto estávamos implementando as medidas citadas acima até voltar ao normal.

Durante períodos de atividades regulares, os nossos dados indicam que 97% das partidas por colocação começam com dez jogadores, e mais de 98,5% das partidas terminam com êxito sem indivíduos perdendo conexão, sem expulsão por ping alto ou servidor offline. O gráfico abaixo ilustra a taxa de sucesso geral nas partidas de Rainbow Six desde o começo do ano (em verde):

sr 1

Entretanto, um bom exemplo de problemas de conectividade que não estão sob nosso controle pode ser visto na seção azul do gráfico. A parte azul acima indica partidas em que um jogador foi expulso devido a ping alto. Com o início das medidas de contenção em março, a porcentagem de expulsões por ping alto foi mais que dobrada devido à carga adicional na Internet do mundo inteiro — um fator que está fora do nosso controle.

Futuro: Nossas equipes estão explorando continuamente novas formas de melhorar a qualidade do serviço em Siege. Eles estão trabalhando sem parar nos bastidores desde o lançamento do Siege e continuam se esforçando até hoje.

Em prol de melhoria de desempenho e resiliência de conectividade, migramos os servidores do nosso jogo para Linux no ano passado. Neste ano, já estamos trabalhando para melhorar a resiliência em caso de interrupções de terceiros. E para melhorar as capacidades de escalar horizontalmente, diminuir tempo de organização de partida e permitir manutenções com menos impacto, estamos trabalhando na transição do nosso backend inteiro para uma arquitetura baseada em microsserviços. Como se pode imaginar, essas mudanças levam tempo. Entretanto, vamos fazer o nosso melhor para suavizar qualquer impacto e ser transparentes com os jogadores, por isso fique de olho nas nossas próximas atualizações conforme essas mudanças entram em vigor.

2.2 POLÍTICA DE LATÊNCIA
Apreensão: Quando o ping de um jogador atinge certo valor e se mantém por um período determinado, o jogador é expulso da partida. O limite para expulsão atual é considerado baixo demais por parte da comunidade.

Desafio: Os nossos limites de ping atuais são baseados em valores observados durante os testes da fase Alfa em 2015. Expulsões por problemas de conectividade e padrões de aviso para o que consideramos lag alto (por impactar a jogabilidade) são baseados naqueles dados. Qualquer valor abaixo de 110 ms é considerado um ping aceitável, enquanto qualquer valor acima disso gera verificações que são aplicadas às ações do jogador antes que sua expulsão seja acionada.

Futuro: Queremos manter uma qualidade geral de desempenho para todos no jogo e ao mesmo tempo permitir alguma margem para potenciais flutuações no curso normal de jogabilidade. As nossas equipes estão empenhadas na redução da latência adicionando servidores em mais regiões — mas isso vai exigir avaliações adequadas para descobrir se a população da respectiva região vai conseguir manter a organização de partidas e continuar agrupando os jogadores dentro de um tempo razoável. Isso também vai depender da capacidade dos nossos provedores de infraestrutura (atualmente, há servidores do Siege em: Oeste Europeu, Europa Setentrional, Norte da África, Hong Kong, Singapura, Japão, Austrália, Oeste dos EUA, Leste dos EUA, Centro-Sul dos EUA e Brasil). Conforme adicionarmos mais regiões e a latência média for reduzida, poderemos reavaliar os nossos dados e revisitar possíveis alterações de limite de ping.

3. CHEATS E COMPORTAMENTO DISRUPTIVO
Trapaças no Siege são e sempre serão um tópico de alta prioridade para nós. Na nossa postagem mais recente de Principais Falhas e Apreensões da Comunidade nós destacamos os resultados de algumas das nossas táticas de curto prazo para mitigar ações de hackers, cheating e paralização de lobby. Vamos continuar comunicando as medidas contra as trapaças desde que isso não comprometa os nossos esforços contra os cheaters e não cause impacto negativo nas soluções implementadas. Também queremos fazer uma postagem mais longa sobre cheating em um futuro próximo.

Quanto ao assunto toxicidade, temos uma postura totalmente contra esse tipo de comportamento. Visamos priorizar interações saudáveis entre jogadores, promover um ambiente positivo no jogo e elevar a comunidade que nos apoia. Neste momento, estamos tentando mitigar comportamento tóxico e disruptivo no jogo com: filtro de conversas, opções de silenciamento, fogo amigo reverso, manter todas as opções de conversa desabilitadas por padrão e a função de cancelamento de partida que será lançada em breve. A batalha contra a toxicidade deve ser um esforço coletivo entre a equipe do Siege e sua comunidade, portanto vamos continuar incentivando o comportamento positivo que queremos ver cada vez mais.

3.1. DDOS/DOS
Apreensão: Problemas com DDoS vem acontecendo de tempos em tempos, pois os cheaters descobrem novas formas de atacar as vulnerabilidades dos nossos sistemas.

Desafios: Estamos totalmente comprometidos a proteger o nosso jogo contra ataques DDoS. Temos uma equipe dedicada ao monitoramento de anomalias na rede e ataques DDoS aos nossos servidores. Porém, cada caso é diferente e muitas vezes requer investigação e solução de parceiros (tanto internos quanto externos). Por exemplo, um aumento recente nos relatórios de DDoS estava ligado a paralização de lobbies — um jeito diferente de corromper o jogo. Trabalhamos para criar uma detecção refinada e um método de rastreamento de quem faz isso e então implementamos uma expulsão automática para os trapaceiros identificados. Para contextualizar, o gráfico abaixo representa o número de ataques DDoS bem-sucedidos aos nossos sistemas.

sr 2

Nos meses anteriores à Ember Rise, o número de ataques DDoS subiram incrivelmente rápido. Começamos a implementação de uma série de medidas para impedir os ataques, incluindo trabalho com provedores externos. Assim que a nossa solução contra DDoS foi ativada, vimos o número de ataques bem-sucedidos cair para quase zero.

A situação continuou estável até pouco tempo atrás, quando um pico nos relatórios de DDoS no começo de março levou à descoberta de técnicas DDoS novas sendo usadas.

Futuro: Vamos continuar monitorando e investigando os ataques DDoS aos nossos servidores. A nossa equipe dedicada a esse problema está recorrendo a diversos recursos para mitigar novos tipos de ataque que descobrimos em colaboração com o nosso provedor externo de nuvem. Vamos providenciar atualizações mais regulares sobre isso em Principais Falhas e Apreensões da Comunidade ou em notas de patch quando tivermos mais informações para compartilhar.

3.2. POTENCIALIZADORES
Apreensão: Potencializadores aumentam a colocação de outros jogadores por meios desonestos ou ilegais. Isso geralmente se dá manipulando nosso sistema de organização de partidas com smurfs e/ou cheating.

Futuro: Para lidar com as contas impulsionadas por trapaças, temos um sistema de 2 etapas que inclui banimento de infratores e, em seguida, diminuição de MMR. Fraudadores banidos causarão perda de MMR, e podemos identificar com bastante segurança quem receber quantidades expressivas de MMR devido a potencializadores. Os jogadores em questão terão seus valores de MMR zerados e deverão jogar novamente as partidas de colocação da temporada. Além disso, para reduzir potencializadores, também implementamos um limite para a diferença máxima de MMR permitida entre membros da equipe. Outra medida para combater o problema é a futura mudança de unificação de MMR. Ela permite que o sistema de organização de partidas identifique jogadores baseando-se em seu MMR global, e não apenas no regional, caso haja troca de servidor (permitindo assim que joguem com o valor redefinido de MMR sazonal por um curto período).

Uma organização de partida injusta causada por jogadores que deturpam seu real nível de habilidade é uma questão em que continuaremos a trabalhar.

3.3. SMURFING
Apreensão: Smurfs geram desequilíbrio na organização de partidas quando usam novas contas para ocultar seu real nível de habilidade. Nosso sistema não usa Nível de Acesso, E/M ou taxa de vitórias para organizar partidas, e sim MMR. Ele é determinado pelo resultado da sua partida, também considerando o MMR da equipe inimiga. Ao esconder o real nível de habilidade, os smurfs afetam o equilíbrio competitivo da partida, restando poucas opções para verificarmos o incidente.

Desafios: Smurfing é uma dificuldade que toda a indústria encara e, em nossa comunidade, prejudica a experiência de jogo.

Nossa equipe de pesquisa de usuários continuará a aperfeiçoar nosso conhecimento e análise desse tópico no futuro, porém uma grande dificuldade é o rastreamento limitado. Smurfs são difíceis de identificar, e precisamos descobrir uma forma de fazer isso antes de modificarmos nosso sistema para combater esses infratores. Atualmente, nossa equipe usa os dados disponíveis para detectar usuários muito atípicos que poderiam ser casos de smurfing. Enquanto isso, seguiremos reforçando medidas como a Verificação de 2 etapas para dificultarmos a multiplicação dessas contas.

3.4. EQUIPES INCOMPLETAS
Desafio: Jogos que começam com número desigual de jogadores são extremamente frustrantes, e esse tipo de desvantagem é contra nosso objetivo de um jogo justo, equilibrado e divertido. Embora o modo Por Colocação permita ao jogador reingressar numa partida, percebemos que isso nem sempre ocorre.

Futuro: Na Temporada 2 do Ano 5, anunciamos a introdução de um novo recurso de cancelamento de partidas. Ele não chegará no lançamento da temporada, mas ao longo dela.

Para criar o novo recurso de cancelamento de partidas, tínhamos de garantir que o sistema não seria explorável e facilmente violado por jogadores que quisessem abandonar uma partida depois de carregá-la. Muitas situações e parâmetros tiveram que ser avaliados para a definição de um critério sob o qual equipes em desvantagem poderiam justificar o cancelamento de uma partida — como desconexões momentâneas que precisam de uma chance de reingresso e como o sistema de votação funcionaria. O cancelamento de partidas dará à equipe desfavorecida a possibilidade de cancelar a rodada quando todos os nossos critérios para possível cancelamento forem atendidos. Estamos animados para compartilhar a novidade na próxima temporada.

3.5. ABUSO NA VOTAÇÃO PARA EXPULSÃO
Apreensão: Ainda que a Votação para Expulsão não seja mais uma opção em jogos Por Colocação, jogadores de partidas rápidas costumam relatar frustração com o abuso da função. A intenção desse recurso é oferecer aos jogadores a opção de remover um companheiro caso ele apresente comportamento problemático. Contudo, o objetivo inicial por vezes é manipulado para gerar toxicidade.

Desafios: Sabemos o quanto alguns tiram proveito da função e concordamos que a reformulação das regras é necessária. O recurso foi aproveitado de diversas maneiras, e estamos avaliando os dados para compreender os vários cenários e como podemos bloquear a abertura para infrações. Identificamos alguns pontos de partida, como a proibição do voto àqueles sob certas sanções, e estamos examinando os conceitos da votação para expulsão com o objetivo de termos uma versão atualizada do recurso na Temporada 4.

3.6. FRUSTAÇÃO COM A REDUÇÃO DE MMR
Apreensão: Por vezes temos feedback acerca da frustração de alguns jogadores que, mesmo vencendo cheaters, ainda sofrem redução de MMR.

Desafios: Quando introduzimos a redução de MMR, havia receio de grandes quedas de MMR aos companheiros desavisados quanto ao cheater, se as partidas onde o cheater perde não fossem canceladas. Como um companheiro desavisado, todas as partidas que vocês venceram juntos são restauradas (assim perdendo MMR), mas todos os jogos que perderam juntos não serão cancelados (portanto, o MMR é mantido).

Este não é um problemas se os companheiros soubessem estar numa equipe com um cheater. Porém, em muitos casos quando contas são invadidas ou quando os companheiros simplesmente não sabem que jogam com um cheater, a situação pode ser muito desagradável para o jogador inocente.

Futuro: Com a restauração de MMR recentemente introduzida, atenuamos parcialmente algumas das maiores possíveis preocupações de redução de MMR mencionadas acima. Quando há uma redução grande de MMR, os jogadores afetados terão seus valores zerados e deverão jogar novamente suas partidas de posicionamento. Continuaremos a explorar futuramente quais outros riscos pode haver revertendo somente os jogos nos quais os cheaters venceram.

3.7. MODO STREAMER
Apreensão: Queremos oferecer aos streamers uma camada de anonimato para que mantenham sua experiência de jogo contra grandes frustrações, tais como stream sniping (quando um indivíduo assiste ao streaming apenas para ter vantagem sobre o streamer).

Desafios: Estamos explorando algumas opções e precisamos encontrar uma solução com a qual ainda pudéssemos identificar internamente jogadores enquanto o Modo Streamer estivesse ativado — ainda garantindo anonimato.

Futuro: Temos uma primeira iteração de uma possível solução discutida com alguns streamers. Embora a recepção inicial tenha sido positiva, temos que garantir que o recurso seja tão blindado quanto possível quanto à segurança para que não haja alternativas que afetem o anonimato, mas ainda nos permitindo rastrear jogadores e seus dados quando uma identidade diferente for usada. Continuaremos a renovar o design e a validação para ver se conseguimos dividir o recurso em entregas mais intermediárias. Estamos comprometidos a oferecer ferramentas onde criadores de conteúdo possam se comunicar com a comunidade e expressar sua paixão com segurança. Ao mesmo tempo, continuamos a receber feedback para garantir que nosso sistema seja robusto e alinhado com as expectativas dos jogadores.

4. ACESSIBILIDADE
Desafios: A acessibilidade do jogador é importante para nós e o crescimento contínuo de Siege. Nossa visão para maior acessibilidade envolve uma solução plena para daltônicos, permitindo aos jogadores a opção de alterar a cor de claymores, miras, luzes de câmeras etc. — considerando ainda de qual time você faz parte para o feedback adequado.

Nossas iniciativas anteriores de soluções para daltônicos não atingiram nossas expectativas quando testadas com esse público. Descobrimos que filtros apenas não eram suficientes para a maioria dos problemas e que, em alguns casos, causavam até maiores dificuldades. Precisávamos implementar uma solução apropriada que oferecesse um amplo leque de seleção de cores. Personalização nesse nível implica a separação de múltiplos elementos para recoloração dinâmica ¬— uma tarefa significativa que decidimos dividir em parcelas menores.

Vamos revisar a questão do daltonismo num futuro próximo. Começaremos aperfeiçoando a experiência de tiro, analisando as cores dos retículos e o que pode ser feito ali. Esperamos ter novidades em breve.

Este é o primeiro passo para implementarmos um sistema flexível e abrangente. Muitos dos recursos de nossa equipe de acessibilidade estão focados em trazer ao jogo o que está definido na Lei de Acessibilidade. Tudo chegará com outras melhorias de acessibilidade não especificamente visando daltonismo.

5. FALHAS NA PROPAGAÇÃO DE FUMAÇA
Apreensão: Atualmente, a propagação de fumaça é incerta e confusa. O cerne do problema é que pequenos ambientes internos não funcionam bem com nosso atual sistema de propagação de fumaça para permitir a disposição realística do gás. Como resultado, temos problemas de clipping frequentes que, no caso do gadget do Smoke, também causam danos.

Desafio: Anteriormente, exploramos soluções para a propagação de fumaça com o sistema de fumaça reformulado do Capitão. Contudo, esse sistema não foi desenvolvido para lidar com grandes camadas de gás denso como o das granadas de gás do gadget de Smoke. Usar o sistema no Capitão com esse tipo de fumaça causaria quedas enormes no desempenho, afetando negativamente a jogabilidade e a experiência do jogador.

Futuro: Testamos algumas pequenas correções e alternativas no sistema, mas os resultados foram insatisfatórios. Concluímos que vamos precisar reformular o sistema de propagação de fumaça, o que exigirá um trabalho considerável nos gráficos e no motor de jogo para garantir boa qualidade visual enquanto mantemos uma taxa de quadros sólida. O caso está suspenso até que tenhamos tempo e recursos para lidar com ele. Comunicaremos assim que tivermos novidades.

6. PROBLEMAS COM DRONES
Apreensão: Várias falhas relacionadas a drones aparecem de vez em quando. Elas variam, mas zumbidos e obtenção de informações são grande parte do problema.

Desafio: Com o crescimento do jogo, o sistema de drones ficou demasiado complexo, com muitos componentes. Às vezes, ao fazermos novas modificações, acabamos afetando acidentalmente outras partes do sistema.

Futuro: Continuaremos investigando falhas de drones conforme forem surgindo e vamos trabalhar para corrigi-las o quanto antes.

Também estamos migrando o sistema de drones para nosso novo sistema de gadget v2, desenvolvido para simplificar, otimizar e separar sistemas. A migração de alguns gadgets para esse sistema foi feita de forma transparente no ano passado, e continuaremos a melhorar todos os nossos gadgets dessa forma para torná-los mais resistentes a exploits e para iteração mais rápida. Testes automáticos também foram adicionados para identificar regressões assim que ocorrerem. Em alguns casos, falhas são causadas por interações complexas com o sistema do drone — ou o jogo como um todo. Esses casos complexos serão tratados assim que a migração para a estrutura de gadgets v2 esteja concluída.

O QUE VEM POR AÍ
Se chegou até aqui, você leu uma longa lista de metas e promessas de longo prazo. Mas temos boas novas para o futuro próximo também. Temos alguns pedidos da comunidade que chegam em breve, como a versão alfa do sistema de replay de partidas, escolher e banir para mapas e vários reparos. Além disso, estamos animados com mais novidades que vêm por aí, como o sistema de reputação, ping 2.0 e muito mais. Estamos ansiosos para seguir desenvolvendo o jogo com todos vocês!

Para atualizações frequentes de Rainbow Six Siege, continue conferindo as notas de atualização, as postagens do desenvolvedor, as notas do designer, os artigos “Principais falhas e apreensões da comunidade” e os painéis de revelação das temporadas. Pretendemos melhorar a comunicação em alguns tópicos essenciais, tais como cheating, conectividade e organização de partida — teremos artigos acerca disso em breve.