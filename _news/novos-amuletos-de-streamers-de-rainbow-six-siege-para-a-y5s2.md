---
title: 'NOVOS AMULETOS DE STREAMERS DE RAINBOW SIX SIEGE PARA A Y5S2!'
excerpt: 'APRESENTAMOS OS NOVOS AMULETOS DE STREAMERS DA Y5S2!
Cada temporada contará com o lançamento de novos amuletos, bem como a volta dos amuletos de streamers de temporadas passadas!'
img: '/news3.JPG'
date: '2020-06-12'
---

## APRESENTAMOS OS NOVOS AMULETOS DE STREAMERS DA Y5S2!

Cada temporada contará com o lançamento de novos amuletos, bem como a volta dos amuletos de streamers de temporadas passadas!

Beaulo

SexyCake

WhiteShark67

PaladinAmber

RETORNANDO
Y5S2StreamerCharmsRet

Y5S2StreamerCharmsRet2

Interro

Pengu

Just9n

Zigueira

Kalera

Kixstar

Gabbo

LagonisR6

Narcoleptic Nugget

Bighead

Varsity

Zander

KingGeorge

Bnans

BikiniBodhi

Jinu6734

Punjistick

MacieJay

Tranth

z1ronic

Shorty

AnneMunition

Matimi0

TangyD

SilphTV

Lil_Lexi

Drid

Salty Academy

Tatted

COMO CONSEGUIR AMULETOS
Esses amuletos estão disponíveis apenas para quem se inscrever no canal do Twitch de seus respectivos streamers usando uma conta Uplay vinculada. Para mais informações sobre como vincular suas contas da Uplay e do Twitch, assim como optar por receber Twitch Drops, consulte essa seção de Perguntas frequentes.

COMO PARTICIPAR
Dada a extensão de nosso ciclo de produção para conteúdo in-game, os participantes da Y5S1, S2 e S3 já foram determinados. Estamos sempre buscando novos criadores de conteúdo para agregar ao programa e iremos explorar novos candidatos em 2020 e 2021. Se sua meta é ver seu amuleto no jogo, usamos os seguintes critérios para iniciar o processo de seleção de candidatos potenciais:

Requisitos a serem considerados

Aproximadamente 350 ou mais espectadores simultâneos – preferível.
Média de 10 streams de Rainbow Six mensais.
Média de 20 horas de streaming de Rainbow Six mensais.
Posição positiva com a Ubisoft/Rainbow Six Siege.
Conteúdo de alta qualidade.
Rainbow Six Siege reserva-se o direito de decisão final.
Consideração para remoção

Menos de 10 streams principais de Rainbow Six Siege em 3 meses.
Queda no nível de qualidade abaixo do considerado aceitável.
Posição comprometida com Rainbow Six Siege/Ubisoft.
Qualquer ação que possa afetar negativamente as marcas R6S/Ubisoft.
Rainbow Six Siege reserva-se o direito de remoção de qualquer amuleto.